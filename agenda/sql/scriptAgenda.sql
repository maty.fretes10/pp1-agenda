DROP DATABASE IF EXISTS  `agenda`;
CREATE DATABASE `agenda` ;
USE agenda;
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Etiqueta` varchar(45) NOT NULL,
  `Domicilio` varchar(45) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Mes` int(4) NOT NULL,
  `Dia` int(4) NOT NULL,  
  `Club` varchar(45) NOT NULL, 
  `Liga` varchar(45) NOT NULL, 
  PRIMARY KEY (`idPersona`)
);

CREATE TABLE `tiposContacto`
(
	`idTipoContacto` int(11) NOT NULL AUTO_INCREMENT,
    `Nombre` varchar(45) NOT NULL,
    PRIMARY KEY (`idTipoContacto`)
);

DROP TABLE IF EXISTS `ligas`;
CREATE TABLE `ligas`
(
	`idLiga` int(11) NOT NULL AUTO_INCREMENT,
    `liga` varchar(45) NOT NULL,
    PRIMARY KEY (`idLiga`)
);

DROP TABLE IF EXISTS `clubes`;
CREATE TABLE `clubes`
(
	`idLiga` int(11) NOT NULL,
	`idClub` int(11) NOT NULL AUTO_INCREMENT,
    `club` varchar(45) NOT NULL,
    PRIMARY KEY (`idClub`)
);

CREATE TABLE `domicilio`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,	
  `calle` varchar(45) NOT NULL,
  `altura` varchar(20) NOT NULL,
  `piso` varchar(45) NOT NULL,
  `depto` varchar(45) NOT NULL,
  `localidad` varchar(45) NOT NULL,
   `provincia` varchar(45) NOT NULL,
  `pais` varchar(45) NOT NULL,
  PRIMARY KEY (`idPersona`)
);

DROP TABLE IF EXISTS `pais`;
CREATE TABLE IF NOT EXISTS `pais` (
  `idpais` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(255) NOT NULL,
  PRIMARY KEY (`idpais`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

DROP TABLE IF EXISTS `provincias`;
CREATE TABLE IF NOT EXISTS `provincias` (
  `idpais` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provincia` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

DROP TABLE IF EXISTS `localidades`;
CREATE TABLE IF NOT EXISTS `localidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_privincia` int(11) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

INSERT INTO agenda.tiposcontacto VALUES (0, '');

INSERT INTO agenda.pais VALUES
(0,''),
(1, 'Argentina'),
(2, 'Uruguay'),
(3, 'Brasil');

INSERT INTO agenda.provincias VALUES
(1,1,'Buenos Aires'),
(1,2,'Misiones'),
(2,3, 'Montevideo'),
(2,4, 'Salto'),
(3,5, 'Goias'),
(3,6, 'Bahia');

INSERT INTO agenda.localidades VALUES
(1,1,'San Miguel'),
(2,1,'Polvorines'),
(3,2,'Iguazu'),
(4,2,'Posadas'),
(5,2,'25 de Mayo'),
(6,3,'PeÃ±arol'),
(7,3,'Sayago'),
(8,4,'Belen'),
(9,4,'Constitucion');

INSERT INTO agenda.ligas VALUES
(1, 'Resto del Mundo'),
(2, 'Premier League'),
(3, 'La Liga'),
(4, 'Serie A'),
(5, 'Brasileirao'),
(6, 'LPF Argentino'),
(7, 'Ligue 1');

INSERT INTO agenda.clubes VALUES
(1, 1, '(sin club)'),
(2, 2, 'Chelsea FC'),
(2, 3, 'Manchester United'),
(3, 4, 'Real Madrid CF'),
(3, 5, 'FC Barcelona'),
(4, 6, 'FC Inter'),
(4, 7, 'AC Milan'),
(5, 8, 'Sao Paulo FC'),
(6, 9, 'Independiente'),
(6, 10, 'River Plate'),
(6, 11, 'Boca Juniors'),
(7, 12, 'Paris Saint Germain');