package modelo;

import java.util.List;

import dto.DomicilioDTO;
import dto.PersonaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.PersonaDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private DomicilioDAO domicilio;	

	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.domicilio = metodo_persistencia.createDomicilioDAO();

	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}
	
	public void agregarDomicilio(DomicilioDTO domNuevo) 
	{
		this.domicilio.insert(domNuevo);
	}
	
	public void editarPersona(PersonaDTO persona_a_editar)
	{
		this.persona.update(persona_a_editar);
	}
	
	public void editarDomicilio(DomicilioDTO editarDomicilio) 
	{
		this.domicilio.update(editarDomicilio);	
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);

	}
	
	public void borrarDomicilio(DomicilioDTO dom_a_eliminar) 
	{
		this.domicilio.delete(dom_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	public List<DomicilioDTO> obtenerDomicilio()
	{
		return this.domicilio.readAll();		
	}
	
	public PersonaDTO obtenerPersona(PersonaDTO persona_a_obtener)
	{
		return this.persona.getId(persona_a_obtener);
	}

	public DomicilioDTO obtenerPersona(DomicilioDTO dom_a_obtener)
	{
		return this.domicilio.getId(dom_a_obtener);
	}
	
}
