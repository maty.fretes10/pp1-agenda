package persistencia.dao.mysql;

import persistencia.conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import dto.ProvinciaDTO;

public class ProvinciaDAOSQL {
	
	private static String readall = "";

    
	private static final String update = "UPDATE provincias SET idpais = ?, provincia = ? WHERE id = ?";
	private static final String insert = "INSERT INTO provincias (idpais, id, provincia) VALUES(?, ?, ?)";
	private static final String getById = "SELECT * FROM provincias WHERE id = ?";
	private static final String delete = "DELETE FROM provincias WHERE id = ?";



	private static final Conexion conexion = Conexion.getConexion();
	
	public boolean update(ProvinciaDTO prov_a_editar) 
	{
		PreparedStatement statement;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setInt(1, prov_a_editar.getIdpais());
			statement.setString(2, prov_a_editar.getNombre());
			statement.setInt(3, prov_a_editar.getId());
			
			
			if(statement.executeUpdate() > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return false;
	}
    
	public boolean insert(ProvinciaDTO prov_a_insertar) 
	{
		PreparedStatement statement;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, prov_a_insertar.getIdpais());
			statement.setInt(2, prov_a_insertar.getId());
			statement.setString(3, prov_a_insertar.getNombre());
			
			
			if(statement.executeUpdate() > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean delete(ProvinciaDTO prov_a_eliminar) 
	{
		PreparedStatement statement;
		int chequeoUpdate=0;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setInt(1, prov_a_eliminar.getId());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public static Vector<ProvinciaDTO> readAll(int idpais)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Vector<ProvinciaDTO> prov = new Vector<ProvinciaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			
			readall = "SELECT * FROM agenda.provincias WHERE idpais =" + idpais;
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				prov.add(getProvincias(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return prov;
	}
	
	private static ProvinciaDTO getProvincias(ResultSet resultSet) throws SQLException
	{
		int idpais = resultSet.getInt("idpais");
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("provincia");

		return new ProvinciaDTO(idpais, id, nombre);
	}
	
	public ProvinciaDTO getById(ProvinciaDTO prov_a_obtener) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ProvinciaDTO prov = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(getById);
			statement.setInt(1, prov_a_obtener.getId());
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				prov = new ProvinciaDTO(resultSet.getInt("idpais"),
										resultSet.getInt("id"),
									  resultSet.getString("provincia"));
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return prov;
	}
}
