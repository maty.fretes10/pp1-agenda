package persistencia.dao.mysql;

import persistencia.conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Vector;

import dto.LocalidadDTO;

public class LocalidadesDAOSQL {
	
	private static String readall = "";
	private static final String update = "UPDATE localidades SET id_privincia = ?, localidad = ? WHERE id = ?";
	private static final String insert = "INSERT INTO localidades (id, id_privincia, localidad) VALUES(?, ?, ?)";
	private static final String delete = "DELETE FROM localidades WHERE id = ?";
	private static final String getById = "SELECT * FROM localidades WHERE id = ?";


	private static final Conexion conexion = Conexion.getConexion();
	
	public boolean update(LocalidadDTO local_a_editar) 
	{
		PreparedStatement statement;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setInt(1, local_a_editar.getId_prov());
			statement.setString(2, local_a_editar.getNombre());
			statement.setInt(3, local_a_editar.getId());
			
			
			if(statement.executeUpdate() > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return false;
	}
    
	public boolean insert(LocalidadDTO localidad_a_insertar) 
	{
		PreparedStatement statement;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, localidad_a_insertar.getId());
			statement.setInt(2, localidad_a_insertar.getId_prov());
			statement.setString(3, localidad_a_insertar.getNombre());		
			
			
			if(statement.executeUpdate() > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean delete(LocalidadDTO localidad_a_eliminar) 
	{
		PreparedStatement statement;
		int chequeoUpdate=0;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setInt(1, localidad_a_eliminar.getId());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public static Vector<LocalidadDTO> readAll(int id_prov)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Vector<LocalidadDTO> loc = new Vector<LocalidadDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			readall = "SELECT * FROM agenda.localidades WHERE id_privincia =" + id_prov;

			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				loc.add(getLocalidad(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		Collections.sort(loc);
		return loc;
	}
	
	private static LocalidadDTO getLocalidad (ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		int idprov = resultSet.getInt("id_privincia");
		String nombre = resultSet.getString("localidad");

		return new LocalidadDTO(id, idprov, nombre);
	}
	
	public LocalidadDTO getById(LocalidadDTO localidad_a_obtener) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		LocalidadDTO loc = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(getById);
			statement.setInt(1, localidad_a_obtener.getId());
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				loc = new LocalidadDTO(resultSet.getInt("id"),
										resultSet.getInt("idprivincia"),
									  resultSet.getString("localidad"));
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return loc;
	}
}