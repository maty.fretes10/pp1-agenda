package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Vector;
import dto.ClubDTO;
import persistencia.conexion.Conexion;


public class ClubDAOSQL
{
	private static final String insert = "INSERT INTO clubes (idLiga, idClub, club) VALUES(?, ?, ?)";
	private static final String update = "UPDATE clubes SET club = ? WHERE idClub = ?";
	private static final String delete = "DELETE FROM clubes WHERE idClub = ?";
	private static String readall = "SELECT * FROM agenda.clubes";
	private static final String getById = "SELECT * FROM clubes WHERE idClub = ?";
	private static final Conexion conexion = Conexion.getConexion();
	
	public boolean update(ClubDTO club_a_editar) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, club_a_editar.getNombreClub());
			statement.setInt(2, club_a_editar.getIdClub());
			
			if(statement.executeUpdate() > 0) {//Si se ejecutó devuelvo true
				conexion.commit();
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return false;
	}

	public boolean insert(ClubDTO club) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		
		try 
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, club.getIdLiga());
			statement.setInt(2, club.getIdClub());
			statement.setString(3, club.getNombreClub());
			
			if(statement.executeUpdate() > 0) { //Si se ejecutó devuelvo true
				conexion.commit();
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(ClubDTO club_a_eliminar) 
	{
		PreparedStatement statement;
		int chequeoUpdate=0;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setInt(1, club_a_eliminar.getIdClub());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	public ClubDTO getById(ClubDTO club_a_obtener) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ClubDTO club = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(getById);
			statement.setInt(1, club_a_obtener.getIdClub());
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				club = new ClubDTO(resultSet.getInt("idLiga"),resultSet.getInt("idClub"),resultSet.getString("club"));
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return club;
	}

	public static Vector<ClubDTO> readAll(int idLiga)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Vector<ClubDTO> clubes = new Vector<ClubDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			readall = "SELECT * FROM agenda.clubes WHERE idLiga =" + idLiga;

			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				clubes.add(getClub(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		Collections.sort(clubes);
		return clubes;
	}
	
	private static ClubDTO getClub (ResultSet resultSet) throws SQLException
	{
		int idLiga = resultSet.getInt("idLiga");
		int idClub = resultSet.getInt("idClub");
		String nombre = resultSet.getString("club");

		return new ClubDTO(idLiga, idClub, nombre);
	}

}
