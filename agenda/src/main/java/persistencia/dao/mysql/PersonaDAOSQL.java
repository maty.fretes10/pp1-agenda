package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.DomicilioDTO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, telefono, etiqueta, domicilio, correo, mes, dia, club, liga) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE personas SET nombre = ?, telefono = ?, etiqueta = ?, domicilio = ?, correo = ?, mes = ?, dia = ?, club = ?, liga = ? WHERE idPersona = ?";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "select * from agenda.personas INNER JOIN agenda.domicilio ON agenda.personas.idPersona = agenda.domicilio.idPersona";	
	private static final Conexion conexion = Conexion.getConexion();
	private static final String conseguirID = "SELECT p.idPersona, p.nombre, p.telefono, p.etiqueta, p.domicilio, p.correo, p.mes, p.dia, p.club, p.liga WHERE idPersona = ?";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.get_etiqueta());
			statement.setString(5, persona.getDom().getCalle() + " " + persona.getDom().getAltura() + " " + persona.getDom().getPiso() + " " + persona.getDom().getDepto());
			statement.setString(6, persona.getCorreo());
			statement.setInt(7, persona.getMes());
			statement.setInt(8, persona.getDia());
			statement.setString(9, persona.getClub());
			statement.setString(10, persona.getLiga());
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean update(PersonaDTO persona) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(update);
			statement.setInt(10, persona.getIdPersona());
			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getTelefono());
			statement.setString(3, persona.get_etiqueta());
			statement.setString(4, persona.getDom().getCalle() + " " + persona.getDom().getAltura());
			statement.setString(5, persona.getCorreo());
			statement.setInt(6, persona.getMes());
			statement.setInt(7, persona.getDia());
			statement.setString(8, persona.getClub());
			statement.setString(9, persona.getLiga());
			
     		if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String etiqueta = resultSet.getString("Etiqueta");
		String nom = resultSet.getString("calle");
		int alt = Integer.parseInt(resultSet.getString("altura"));
		int pis = Integer.parseInt(resultSet.getString("piso"));
		int dep = Integer.parseInt(resultSet.getString("depto"));
		String loc = resultSet.getString("localidad");
		String prov = resultSet.getString("provincia");
		String pais = resultSet.getString("pais");
		String correo = resultSet.getString("Correo");
		int dia = resultSet.getInt("dia");
		int mes = resultSet.getInt("mes");
		String club = resultSet.getString("club");
		String liga = resultSet.getString("liga");
		
		DomicilioDTO dom = new DomicilioDTO(id,nom,alt,pis,dep,loc, prov, pais);
		
		return new PersonaDTO(id, nombre, tel, etiqueta, dom, correo, dia, mes, club, liga);
	}
	public PersonaDTO getById(PersonaDTO persona_a_obtener) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		PersonaDTO persona = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(conseguirID);
			statement.setInt(1, persona_a_obtener.getIdPersona());
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				persona = new PersonaDTO(resultSet.getInt("idPersona"),
										 resultSet.getString("Nombre"), 
										 resultSet.getString("Telefono"),
										 resultSet.getString("Etiqueta"),
										 new DomicilioDTO( resultSet.getInt("idPersona"),
													resultSet.getString("Calle"),
													Integer.parseInt(resultSet.getString("Altura")),
													Integer.parseInt(resultSet.getString("Piso")),
													Integer.parseInt(resultSet.getString("Depto")),
													resultSet.getString("Localidad"),
													resultSet.getString("Provincia"),
													resultSet.getString("Pais")
										),
										 resultSet.getString("Correo"),
										 resultSet.getInt("Mes"),
										 resultSet.getInt("Dia"),
										 resultSet.getString("Club"),
										 resultSet.getString("Liga"));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return persona;
	}
	@Override
	public PersonaDTO getId(PersonaDTO persona_a_obtener) {
		// TODO Auto-generated method stub
		return null;
	}
}
