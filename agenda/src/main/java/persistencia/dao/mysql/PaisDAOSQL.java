package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import dto.PaisDTO;
import persistencia.conexion.Conexion;

public class PaisDAOSQL 
{
		private static String readall = "SELECT * FROM agenda.pais";
		
		private static final String update = "UPDATE pais SET pais = ? WHERE idpais = ?";
		private static final String insert = "INSERT INTO pais (idpais, pais) VALUES(?, ?)";
		private static final String getById = "SELECT * FROM pais WHERE idpais = ?";
		private static final String delete = "DELETE FROM pais WHERE idpais = ?";
		private static final Conexion conexion = Conexion.getConexion();
		
		
		
		public boolean update(PaisDTO pais_a_editar) 
		{
			PreparedStatement statement;
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(update);
				statement.setString(1, pais_a_editar.getNombre());
				statement.setInt(2, pais_a_editar.getId());
				
				
				if(statement.executeUpdate() > 0) //Si se ejecutó devuelvo true
					return true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}

			return false;
		}
		
		public boolean insert(PaisDTO pais_a_insertar) 
		{
			PreparedStatement statement;
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(insert);
				statement.setInt(1, pais_a_insertar.getId());
				statement.setString(2, pais_a_insertar.getNombre());
				
				if(statement.executeUpdate() > 0) //Si se ejecutó devuelvo true
					return true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return false;
		}

		public boolean delete(PaisDTO pais_a_eliminar) 
		{
			PreparedStatement statement;
			int chequeoUpdate=0;
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(delete);
				statement.setInt(1, pais_a_eliminar.getId());
				chequeoUpdate = statement.executeUpdate();
				if(chequeoUpdate > 0) //Si se ejecut� devuelvo true
					return true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return false;
		}
		
		public static Vector<PaisDTO> readAll()
		{
			PreparedStatement statement;
			ResultSet resultSet; //Guarda el resultado de la query
			Vector<PaisDTO> pais = new Vector<PaisDTO>();
			Conexion conexion = Conexion.getConexion();
			try 
			{

				statement = conexion.getSQLConexion().prepareStatement(readall);
				resultSet = statement.executeQuery();
				while(resultSet.next())
				{
					pais.add(getPaises(resultSet));
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return pais;
		}
		
		private static PaisDTO getPaises(ResultSet resultSet) throws SQLException
		{
			int idpais = resultSet.getInt("idpais");
			String nombre = resultSet.getString("pais");

			return new PaisDTO(idpais, nombre);
		}
		
		public PaisDTO getById(PaisDTO pais_a_obtener) 
		{
			PreparedStatement statement;
			ResultSet resultSet; //Guarda el resultado de la query
			PaisDTO pais = null;
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(getById);
				statement.setInt(1, pais_a_obtener.getId());
				resultSet = statement.executeQuery();
				while(resultSet.next())
				{
					pais = new PaisDTO(resultSet.getInt("idpais"),
										  resultSet.getString("pais"));
				}
			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}

			return pais;
		}

}
