package persistencia.dao.mysql;


	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.ArrayList;
	import java.util.List;

	import persistencia.conexion.Conexion;
	import persistencia.dao.interfaz.DomicilioDAO;
	import dto.DomicilioDTO;

	public class DomicilioDAOSQL implements DomicilioDAO
	{
		private static final String insert = "INSERT INTO domicilio(idPersona,calle,altura,piso,depto,localidad,provincia,pais) VALUES(?, ?, ?, ?, ?, ?,?,?)";
		private static final String update = "UPDATE domicilio SET calle = ?, altura = ?, piso = ?, depto = ?, localidad = ?, provincia = ?, pais = ? WHERE idPersona = ?";		
		private static final String delete = "DELETE FROM domicilio WHERE idPersona = ?";
		private static final String readall = "SELECT * FROM domicilio";
		private static final String conseguirID = "SELECT d.idPersona, d.calle, d.altura, d.piso, d.depto, d.localidad, where idPersona = ?";
		private static final Conexion conexion = Conexion.getConexion();

			
		public boolean insert(DomicilioDTO domicilio)
		{
			PreparedStatement statement;
			Connection conexion = Conexion.getConexion().getSQLConexion();
			boolean isInsertExitoso = false;
			try
			{
				statement = conexion.prepareStatement(insert);
				statement.setInt(1, domicilio.getIdpersona());
				statement.setString(2, domicilio.getCalle());
				statement.setInt(3, domicilio.getAltura());
				statement.setInt(4, domicilio.getPiso());
				statement.setInt(5, domicilio.getDepto());
				statement.setString(6, domicilio.getLocalidad());
				statement.setString(7, domicilio.getProvincia());
				statement.setString(8, domicilio.getPais());
				

				if(statement.executeUpdate() > 0)
				{
					conexion.commit();
					isInsertExitoso = true;
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			
			return isInsertExitoso;
		}
		
		public boolean delete(DomicilioDTO dom_eliminar)
		{
			PreparedStatement statement;
			Connection conexion = Conexion.getConexion().getSQLConexion();
			boolean isdeleteExitoso = false;
			try 
			{
				statement = conexion.prepareStatement(delete);
				statement.setInt(1, dom_eliminar.getIdpersona());
				if(statement.executeUpdate() > 0)
				{
					conexion.commit();
					isdeleteExitoso = true;
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return isdeleteExitoso;
		}
		
		public List<DomicilioDTO> readAll()
		{
			PreparedStatement statement;
			ResultSet resultSet; //Guarda el resultado de la query
			ArrayList<DomicilioDTO> dom = new ArrayList<DomicilioDTO>();
			Conexion conexion = Conexion.getConexion();
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(readall);
				resultSet = statement.executeQuery();
				while(resultSet.next())
				{
					dom.add(getDomicilio(resultSet));
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return dom;
		}
		
		private DomicilioDTO getDomicilio(ResultSet resultSet) throws SQLException
		{
			String calle = resultSet.getString("Calle");
			int altura = resultSet.getInt("Altura");
			int piso = resultSet.getInt("Piso");
			int depto = resultSet.getInt("Depto");
			String localidad = resultSet.getString("Localidad");
			String provincia = resultSet.getString("Provincia");
			String pais = resultSet.getString("Pais");
			
					
			return new DomicilioDTO(resultSet.getInt("idPersona"),calle, altura, piso, depto, localidad,provincia, pais);
		}

		public boolean update(DomicilioDTO dom_editar) 
		{
			PreparedStatement statement;
			Connection conexion = Conexion.getConexion().getSQLConexion();
			boolean isInsertExitoso = false;
			try
			{
				statement = conexion.prepareStatement(update);
				statement.setInt(8, dom_editar.getIdpersona());
				statement.setString(1, dom_editar.getCalle());
				statement.setInt(2, dom_editar.getAltura());
				statement.setInt(3, dom_editar.getPiso());
				statement.setInt(4, dom_editar.getDepto());
				statement.setString(5, dom_editar.getLocalidad());
				statement.setString(6, dom_editar.getProvincia());
				statement.setString(7, dom_editar.getPais());
				

				
				if(statement.executeUpdate() > 0)
				{
					conexion.commit();
					isInsertExitoso = true;
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			
			return isInsertExitoso;
		}
		
		public DomicilioDTO getById(DomicilioDTO dom_obtener) 
		{
			PreparedStatement statement;
			ResultSet resultSet; //Guarda el resultado de la query
			DomicilioDTO domicilio = null;
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(conseguirID);
				statement.setInt(1, dom_obtener.getIdpersona());
				resultSet = statement.executeQuery();
				
				while(resultSet.next())
				{
					domicilio = 
								new DomicilioDTO( resultSet.getInt("idPersona"),
										resultSet.getString("Calle"),
										Integer.parseInt(resultSet.getString("Altura")),
										Integer.parseInt(resultSet.getString("Piso")),
										Integer.parseInt(resultSet.getString("Depto")),
										resultSet.getString("Localidad"),
										resultSet.getString("Provincia"),
										resultSet.getString("Pais")
							);
						
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return domicilio;
		}

		@Override
		public DomicilioDTO getId(DomicilioDTO persona_a_obtener) {
			// TODO Auto-generated method stub
			return null;
		}
}
