package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import dto.LigaDTO;
import persistencia.conexion.Conexion;

public class LigaDAOSQL {
	private static final String insert = "INSERT INTO ligas (idLiga, liga) VALUES(?, ?)";
	private static final String update = "UPDATE ligas SET liga = ? WHERE idLiga = ?";
	private static final String delete = "DELETE FROM ligas WHERE idLiga = ?";
	private static String readall = "SELECT * FROM agenda.ligas";
	private static final String getById = "SELECT * FROM ligas WHERE idLiga = ?";
	private static final Conexion conexion = Conexion.getConexion();
	
	public boolean update(LigaDTO liga_a_editar) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, liga_a_editar.getNombre());
			statement.setInt(2, liga_a_editar.getIdLiga());
			
			
			if(statement.executeUpdate() > 0) { //Si se ejecutó devuelvo true
				conexion.commit();
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return false;
	}

	public boolean insert(LigaDTO liga) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, liga.getIdLiga());
			statement.setString(2, liga.getNombre());

			if(statement.executeUpdate() > 0) {//Si se ejecutó devuelvo true
				conexion.commit();
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(LigaDTO liga_a_eliminar) 
	{
		PreparedStatement statement;
		int chequeoUpdate=0;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setInt(1, liga_a_eliminar.getIdLiga());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	public LigaDTO getById(LigaDTO liga_a_obtener) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		LigaDTO liga = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(getById);
			statement.setInt(1, liga_a_obtener.getIdLiga());
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				liga = new LigaDTO(resultSet.getInt("idLiga"),resultSet.getString("liga"));
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return liga;
	}

	public static Vector<LigaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Vector<LigaDTO> l = new Vector<LigaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				l.add(getLiga(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return l;
	}
	
	private static LigaDTO getLiga (ResultSet resultSet) throws SQLException
	{
		int idLiga =resultSet.getInt("idLiga");
		String nombre = resultSet.getString("liga");

		return new LigaDTO(idLiga, nombre);
	}
}
