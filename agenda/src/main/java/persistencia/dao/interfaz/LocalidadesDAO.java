package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

public interface LocalidadesDAO 
{
	public boolean update(LocalidadDTO localidad_a_editar);
	
	public boolean insert(LocalidadDTO localidad_add);

	public boolean delete(LocalidadDTO localidad_borrar);
	
	public LocalidadDTO getById(LocalidadDTO localidad_a_obtener);
	
	public List<LocalidadDTO> readAll();
}