package persistencia.dao.interfaz;

import java.util.List;

import dto.PaisDTO;

public interface PaisDAO 
{
	public boolean update(PaisDTO pais_a_editar);
	
	public boolean insert(PaisDTO pais_add);

	public boolean delete(PaisDTO pais_borrar);
	
	public PaisDTO getById(PaisDTO pais_a_obtener);
	
	public List<PaisDTO> readAll();
}