package persistencia.dao.interfaz;

import java.util.List;

import dto.LigaDTO;

public interface LigaDAO 
{
	public boolean update(LigaDTO liga_a_editar);
	
	public boolean insert(LigaDTO liga);

	public boolean delete(LigaDTO liga_a_eliminar);
	
	public LigaDTO getById(LigaDTO liga_a_obtener);
	
	public List<LigaDTO> readAll();
}
