package persistencia.dao.interfaz;

import java.util.List;

import dto.ClubDTO;

public interface ClubDAO 
{
	public boolean update(ClubDTO club_a_editar);
	
	public boolean insert(ClubDTO club);

	public boolean delete(ClubDTO club_a_eliminar);
	
	public ClubDTO getById(ClubDTO club_a_obtener);
	
	public List<ClubDTO> readAll();
}