package persistencia.dao.interfaz;

import java.util.List;

import dto.ProvinciaDTO;

public interface ProvinciaDAO 
{
	public boolean update(ProvinciaDTO provincia_a_editar);
	
	public boolean insert(ProvinciaDTO provincia_add);

	public boolean delete(ProvinciaDTO provincia_borrar);
	
	public ProvinciaDTO getById(ProvinciaDTO provincia_a_obtener);
	
	public List<ProvinciaDTO> readAll();
}