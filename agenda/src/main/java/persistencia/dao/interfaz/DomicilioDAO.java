package persistencia.dao.interfaz;

import java.util.List;

import dto.DomicilioDTO;

public interface DomicilioDAO 
{
	public boolean update(DomicilioDTO domicilio);
	
	public boolean insert(DomicilioDTO domicilio);

	public boolean delete(DomicilioDTO com_eliminar);
	
	public List<DomicilioDTO> readAll();
	
	public DomicilioDTO getId(DomicilioDTO domicilio_eliminar);

}
