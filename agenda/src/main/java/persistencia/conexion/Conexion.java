package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import presentacion.vista.ConexionMySQL;

public class Conexion 
{

	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);	
	
	private Conexion()
	{
		try
		{
			BasicConfigurator.configure();
			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
			this.connection = DriverManager.getConnection("jdbc:mysql://" + ConexionMySQL.getTxtIP() + ":" + ConexionMySQL.getTxtPuerto()+"/agenda?useSSL=false", ConexionMySQL.getTxtUser(), ConexionMySQL.getTxtContra());
			this.connection.setAutoCommit(false);
			log.info("Conexi�n exitosa");
			ConexionMySQL.setEstadoConexion(true);
		}
		catch(Exception e)
		{
			log.error("Conexi�n fallida", e);
			ConexionMySQL.setEstadoConexion(false);
		}
	}
	
	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexi�n!", e);
		}
		instancia = null;
	}
}

