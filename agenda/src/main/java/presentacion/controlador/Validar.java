package presentacion.controlador;

import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

public class Validar {

	
	public static boolean Campos(String nombre, String tel, String etiqueta, String email, int dia, int mes,
			String calle, int altura, int piso, int depto, String localidad, String provincia, String pais) {

		if(tel.isBlank()) {
			JOptionPane.showMessageDialog(null, "El campo telefono es obligatorio",  "Campo obligatorio", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
		
		if(!Fechas(dia, mes)) {
			JOptionPane.showMessageDialog(null, "Fecha inexistente",  "Cambiar fecha", JOptionPane.INFORMATION_MESSAGE);
			return false;	
		}
		
		if(!Email(email)) {
			JOptionPane.showMessageDialog(null, "Formato de email incorrecto",  "Cambiar email", JOptionPane.INFORMATION_MESSAGE);
			return false;	
		}

		return true; 
	}

	
	public static boolean Fechas(int dia, int mes) 
	{
		if(dia < 0 || dia > 31) { return false; }
		if(mes == 2 && dia > 29 ) { return false; }
		if(dia == 31 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) { return false; }
		
		return true;
	}
	
	public static void SoloNumeros(java.awt.event.KeyEvent e) 
	{
		char car = e.getKeyChar();
		if (car == ' ') {car = 0;}
		
		if((car < '0' || car > '9') && (car!=(char)KeyEvent.VK_BACK_SPACE)) 
		{
			e.consume();
			JOptionPane.showMessageDialog(null, "Solo ingresar numeros", "Validar numeros", JOptionPane.INFORMATION_MESSAGE);
		}	
	}
	
	
	public static boolean Email(String correo) 
	{
		if(correo.length() == 0) {
			return true;
		}
		
		Pattern pat = null;
		Matcher mat = null;
		
		pat = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		mat = pat.matcher(correo);
			
		return mat.find();
	}
}
