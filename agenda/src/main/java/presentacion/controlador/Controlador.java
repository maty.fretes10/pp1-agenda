package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;
import presentacion.vista.Ayuda.Ayuda;
import presentacion.vista.Ayuda.ConstanteAyuda;
import dto.DomicilioDTO;
import dto.PersonaDTO;

public class Controlador implements ActionListener
{
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private VentanaPersona ventanaPersona; 
		private Ayuda ayuda;
		private int fila_editar;
		private Agenda agenda;
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnEditar().addActionListener(e->editarPersona(e));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnReporte().addActionListener(r->{
				try {
					mostrarReporte(r);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			});
			this.vista.getBtnAyuda().addActionListener(ay -> ayudar(ay));
			this.agenda = agenda;
		}
		
		private void ventanaAgregarPersona(ActionEvent a) 
		{
			this.ventanaPersona = new VentanaPersona(this, "Agregar", null, null);
			this.ventanaPersona.mostrarVentana();
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));
		}
		

		private void ventanaEditarPersona(ActionEvent e, PersonaDTO per, DomicilioDTO dom) 
		{
			this.ventanaPersona = new VentanaPersona(this, "Editar", per, dom);
			this.ventanaPersona.mostrarVentana();
		}

		private void guardarPersona(ActionEvent p) 
		{
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String tel = ventanaPersona.getTxtTelefono().getText();
			String email = ventanaPersona.getTxtCorreo();
			String etiqueta = ventanaPersona.getTxtEtiqueta();
			String calle = ventanaPersona.getTxtCalle().getText();
			int altura = ventanaPersona.getTxtAltura();
			int piso = ventanaPersona.getTxtPiso();
			int depto = ventanaPersona.getTxtDepto();
			int dia = ventanaPersona.getDia();
			int mes = ventanaPersona.getMes();
			
			if(nombre == null) {nombre = "";}
			if(email == null) {email = "";}
			if(etiqueta == null) {etiqueta = "Sin etiqueta";}
			String pais; 
			try { pais = ventanaPersona.getPais().getNombre();
			} catch (Exception e) { pais = ""; }

			String provincia; 
			try { provincia = ventanaPersona.getProvincia().getNombre();
			} catch (Exception e) { provincia = ""; }
			
			String localidad; 
			try { localidad = ventanaPersona.getLocalidad().getNombre();
			} catch (Exception e) { localidad = ""; }
			
			if(calle == null) {calle = "";}
				
			if(Validar.Campos(nombre, tel, etiqueta, email, dia, mes,calle,altura,piso,depto,localidad, provincia, pais)) {
				DomicilioDTO nuevoDomicilio = new DomicilioDTO(0,calle,altura,piso,depto,localidad, provincia, pais);
				String club = "(sin club)";
				String liga = "(sin liga)";
				if(club != "" && liga != "") {
					club = VentanaPersona.lblClub.getText();
					liga = VentanaPersona.lblLiga.getText();
				}
				PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, etiqueta, nuevoDomicilio, email, dia, mes, club, liga);
				this.agenda.agregarPersona(nuevaPersona);
				this.agenda.agregarDomicilio(nuevoDomicilio);
				this.refrescarTabla();
				this.ventanaPersona.cerrar();
			}
		}

		private void editarPersona(ActionEvent e) 
		{
			try 
			{	
			int[] filas_seleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			
				if(filas_seleccionadas.length == 1)
				{				
					fila_editar = this.personasEnTabla.get(filas_seleccionadas[0]).getIdPersona();	
					ventanaEditarPersona(e, this.personasEnTabla.get(filas_seleccionadas[0]), this.personasEnTabla.get(filas_seleccionadas[0]).getDom());
					this.ventanaPersona.getBtnEditarPersona().addActionListener(ep->edit(ep, fila_editar));					
					refrescarTabla();
				}
				
			}catch(Exception c){
				System.out.println(c);
				JOptionPane.showMessageDialog(null, "No hay contactos que editar");
			}
		}
		
		private void edit(ActionEvent e, int fila_eliminar) {
			if(e.getSource() == this.ventanaPersona.getBtnEditarPersona())
			{
				PersonaDTO editarPersona = this.ventanaPersona.getDatosPersona();
				DomicilioDTO editarDomicilio = this.ventanaPersona.getDatosDomicilio();
				
				if(editarPersona.getNombre() == null) { editarPersona.setNombre(""); }
				if(editarPersona.getCorreo() == null) { editarPersona.setCorreo(""); }
				if(editarPersona.get_etiqueta() == null) { editarPersona.set_etiqueta("(sin_etiqueta)");}
				if(editarPersona.getClub() == null) { editarPersona.setClub("(sin club)"); }
				if(editarPersona.getLiga() == null) { editarPersona.setLiga("(sin liga)"); }

				if(editarPersona.getDom().getPais().isBlank()) {
					editarDomicilio.setProvincia("");
					editarDomicilio.setLocalidad("");
				}
				if(editarPersona.getDom().getProvincia().isBlank()) {
					editarDomicilio.setPais("");
					editarDomicilio.setLocalidad("");
				}
				if(editarPersona.getDom().getLocalidad().isBlank()) {
					editarDomicilio.setProvincia("");
					editarDomicilio.setPais("");
				}
				
				if(editarPersona.getDom().getCalle() == null) {editarPersona.getDom().setCalle("");}
				
				
				if(Validar.Campos(editarPersona.getNombre(), editarPersona.getTelefono(), editarPersona.get_etiqueta(), 
						editarPersona.getCorreo(), editarPersona.getDia(), editarPersona.getMes(), editarDomicilio.getCalle(),
						editarDomicilio.getAltura(),editarDomicilio.getPiso(),editarDomicilio.getDepto(),editarDomicilio.getLocalidad(), 
						editarDomicilio.getProvincia(), editarDomicilio.getPais())) {
					
					this.agenda.editarPersona(editarPersona);
					this.agenda.editarDomicilio(editarDomicilio);
					this.refrescarTabla();
					this.ventanaPersona.cerrar();
				}
			}
		}
		
		private void mostrarReporte(ActionEvent r) throws IOException 
		{
			ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
			reporte.mostrar();	
		}
			
		public void borrarPersona(ActionEvent s)
		{
			int[] filas_seleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			
				if(filas_seleccionadas.length == 1)
				{				
					this.agenda.borrarDomicilio(this.personasEnTabla.get(filas_seleccionadas[0]).getDom());
					this.agenda.borrarPersona(this.personasEnTabla.get(filas_seleccionadas[0]));
					refrescarTabla();
				}
		}
		
		public void ayudar(ActionEvent ay) {
			this.ayuda = new Ayuda(ConstanteAyuda.VISTA);
			this.ayuda.show();
		}
		
		public void inicializar()
		{
			this.refrescarTabla();
			this.vista.show();
		}
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.vista.llenarTabla(this.personasEnTabla);
		}

		@Override
		public void actionPerformed(ActionEvent e) { }
		

		
		
		
}
