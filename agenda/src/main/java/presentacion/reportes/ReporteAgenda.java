package presentacion.reportes;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import dto.PersonaDTO;

public class ReporteAgenda
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(ReporteAgenda.class);
	
//	Recibe la lista de personas para armar el reporte
 	public ReporteAgenda(List<PersonaDTO> personas) throws IOException {
 		Map<String, Object> parametersMap = new HashMap<String, Object>();
 		parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
 		try  {
 			File n = new File("");
 			String dir = n.getAbsolutePath()+"\\reportes\\ReporteAgenda.jrxml";
 			this.reporte = JasperCompileManager.compileReport(dir);
 			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
 					new JRBeanCollectionDataSource(personas));
 			
 			System.out.println("Se cargo correctamente el reporte");
 			log.info("Se cargo correctamente el reporte");
 		} 
    	catch (JRException ex) 
    	{
 			System.out.println("Ocurrio un error mientras se cargaba el archivo ReporteAgenda.Jasper  " + ex);
 			log.error("Ocurrio un error mientras se cargaba el archivo ReporteAgenda.Jasper", ex);
 		}
 	}

    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	