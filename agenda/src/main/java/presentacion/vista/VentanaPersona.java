package presentacion.vista;

import java.awt.Image;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;
import dto.ClubDTO;
import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.ProvinciaDTO;
import persistencia.dao.mysql.LocalidadesDAOSQL;
import persistencia.dao.mysql.PaisDAOSQL;
import persistencia.dao.mysql.ProvinciaDAOSQL;
import persistencia.dao.mysql.TipoContactoDAOSQL;
import presentacion.controlador.Controlador;
import presentacion.controlador.Validar;
import presentacion.vista.ABMlocalidades.VistaABMLocalidades;
import presentacion.vista.Ayuda.Ayuda;
import presentacion.vista.Ayuda.ConstanteAyuda;
import presentacion.vista.ABMclubes.VistaClubes;
import java.awt.Color;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import com.toedter.calendar.JMonthChooser;
import java.awt.Font;
import javax.swing.SwingConstants;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JButton btnAgregarPersona;
	private JButton btnEditarPersona;
	private String etiqueta;
	private TipoContactoDTO _etiqueta;
	@SuppressWarnings("rawtypes")
	private static JComboBox CB_Etiquetas;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDepto;
	private JComboBox<LocalidadDTO> CB_Localidad;
	private static JComboBox<PaisDTO> CB_Pais;
	private JComboBox<ProvinciaDTO> CB_Provincia;
	private LocalidadDTO localidad;
	private ProvinciaDTO provincia;
	private PaisDTO pais;
	private Controlador controlador;
	private int idpersona;
	private JTextField txtCorreo;
	private JMonthChooser mes;
	private JTextField txtDia;
	private JButton btnAgregarClub_persona;
	private JButton btnEditarClub_persona;
	private ClubDTO clubAddPersona;
	public static JLabel lblClub;
	public static JLabel lblLiga;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public VentanaPersona(Controlador controlador, String accion, PersonaDTO persona, DomicilioDTO domicilio) 
	{
		super();
		setResizable(false);
		this.controlador = controlador;
		
		setIdPersona(0);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 343, 640);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 327, 601);
		contentPane.add(panel);
		panel.setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setToolTipText("Nombre");
		txtNombre.setBounds(62, 27, 217, 22);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setToolTipText("Tel\u00E9fono");
		txtTelefono.setBounds(62, 76, 217, 22);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		txtTelefono.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				Validar.SoloNumeros(e);
			}
		});
		
		
		JLabel imagenName = new JLabel("");
		imagenName.setBounds(10, 17, 32, 32);
		crearImagen(imagenName, "name.png");
		panel.add(imagenName);
		
		JLabel imagenPhone = new JLabel("");
		imagenPhone.setBounds(10, 66, 32, 32);
		crearImagen(imagenPhone, "phone.png");
		panel.add(imagenPhone);
		
		JLabel lblEtiqueta = new JLabel("Etiqueta");
		lblEtiqueta.setBounds(20, 382, 71, 14);
		panel.add(lblEtiqueta);
		
		CB_Etiquetas = new JComboBox();
		CB_Etiquetas.setBackground(Color.WHITE);
		CB_Etiquetas.setBounds(62, 399, 248, 32);
		CB_Etiquetas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				etiqueta = CB_Etiquetas.getSelectedItem().toString();
				set_etiqueta((TipoContactoDTO) CB_Etiquetas.getSelectedItem());
			}
		});
		panel.add(CB_Etiquetas);
		ActualizarEtiquetas();
		
		JButton btnEditarEtiquetas = new JButton("");
		btnEditarEtiquetas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VistaTiposContacto abmcontacto = new VistaTiposContacto();
				abmcontacto.show();
			}
		});
		btnEditarEtiquetas.setBackground(new Color(255, 255, 224));
		btnEditarEtiquetas.setBounds(10, 399, 32, 32);
		cambiarIconoBotones(btnEditarEtiquetas, "label.png");
		btnEditarEtiquetas.setToolTipText("Editar");
		panel.add(btnEditarEtiquetas);
		
		JButton btnEditarLocalidad = new JButton("");
		btnEditarLocalidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VistaABMLocalidades abmlocal = new VistaABMLocalidades();
				abmlocal.show();
			}
		});
		btnEditarLocalidad.setBackground(Color.WHITE);
		btnEditarLocalidad.setBounds(10, 193, 32, 32);
		cambiarIconoBotones(btnEditarLocalidad, "maps.png");
		btnEditarLocalidad.setToolTipText("Editar");
		panel.add(btnEditarLocalidad);
		
		CB_Pais = new JComboBox();
		CB_Pais.setBackground(Color.WHITE);
		CB_Pais.setForeground(Color.BLACK);
		CB_Pais.setBounds(45, 203, 77, 22);
		panel.add(CB_Pais);
		
		CB_Provincia = new JComboBox();
		CB_Provincia.setForeground(Color.BLACK);
		CB_Provincia.setBackground(Color.WHITE);
		CB_Provincia.setBounds(125, 203, 84, 22);
		panel.add(CB_Provincia);
		
		CB_Localidad = new JComboBox();
		CB_Localidad.setForeground(Color.BLACK);
		CB_Localidad.setBackground(Color.WHITE);
		CB_Localidad.setBounds(212, 203, 98, 22);
		panel.add(CB_Localidad);
		
		JLabel lbl_pais = new JLabel("Pais:");
		lbl_pais.setBounds(45, 188, 57, 14);
		panel.add(lbl_pais);
		
		JLabel lbl_provincia = new JLabel("Provincia:");
		lbl_provincia.setBounds(128, 188, 57, 14);
		panel.add(lbl_provincia);
		
		JLabel lbl_ciudad = new JLabel("Ciudad:");
		lbl_ciudad.setBounds(212, 188, 57, 14);
		panel.add(lbl_ciudad);
		
		JLabel imagen_home = new JLabel("");
		imagen_home.setBackground(Color.WHITE);
		imagen_home.setBounds(10, 240, 32, 32);
		crearImagen(imagen_home, "home.png");
		panel.add(imagen_home);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(46, 250, 132, 22);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField();
		txtAltura.setColumns(10);
		txtAltura.setBounds(180, 250, 55, 22);
		txtAltura.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				Validar.SoloNumeros(e);
			}
		});
		panel.add(txtAltura);
		
		txtPiso = new JTextField();
		txtPiso.setColumns(10);
		txtPiso.setBounds(237, 250, 32, 22);
		txtPiso.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				Validar.SoloNumeros(e);
			}
		});
		panel.add(txtPiso);
		
		txtDepto = new JTextField();
		txtDepto.setColumns(10);
		txtDepto.setBounds(272, 250, 32, 22);
		txtDepto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				Validar.SoloNumeros(e);
			}
		});
		panel.add(txtDepto);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(45, 236, 46, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(180, 236, 46, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(237, 236, 46, 14);
		panel.add(lblPiso);
		
		JLabel lblDepto = new JLabel("Depto");
		lblDepto.setBounds(272, 236, 46, 14);
		panel.add(lblDepto);
		
		JLabel imagenEmail = new JLabel("");
		imagenEmail.setBounds(10, 115, 32, 32);
		crearImagen(imagenEmail, "email.png");
		panel.add(imagenEmail);
		
		txtCorreo = new JTextField();
		txtCorreo.setToolTipText("Direcci\u00F3n de correo electr\u00F3nico");
		txtCorreo.setColumns(10);
		txtCorreo.setBounds(62, 125, 217, 22);
		panel.add(txtCorreo);
		setPais((dto.PaisDTO) CB_Pais.getSelectedItem());
		
		
		JLabel lblDomicilio = new JLabel("Datos del domicilio");
		lblDomicilio.setBounds(20, 166, 139, 14);
		panel.add(lblDomicilio);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 158, 308, 7);
		panel.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 283, 308, 7);
		panel.add(separator_1);
		
		JLabel imagenCumple = new JLabel("");
		imagenCumple.setBackground(Color.WHITE);
		imagenCumple.setBounds(12, 325, 32, 32);
		crearImagen(imagenCumple, "cumple.png");
		panel.add(imagenCumple);
		
		JLabel lblNewLabel = new JLabel("Fecha de cumplea\u00F1os");
		lblNewLabel.setBounds(20, 291, 114, 14);
		panel.add(lblNewLabel);
		
		JSeparator separator_1_1 = new JSeparator();
		separator_1_1.setBounds(10, 372, 307, 7);
		panel.add(separator_1_1);
		
		JSeparator separator_1_1_1 = new JSeparator();
		separator_1_1_1.setBounds(10, 442, 308, 7);
		panel.add(separator_1_1_1);
		
		JLabel lblDia = new JLabel("D\u00EDa");
		lblDia.setBounds(62, 316, 46, 14);
		panel.add(lblDia);
		
		JLabel lblMes = new JLabel("Mes");
		lblMes.setBounds(113, 315, 46, 14);
		panel.add(lblMes);
		
		mes = new JMonthChooser();
		mes.getComboBox().setFont(new Font("Tahoma", Font.PLAIN, 13));
		mes.setBounds(113, 329, 98, 32);
		panel.add(mes);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(62, 11, 46, 14);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Telefono");
		lblNewLabel_1_1.setBounds(62, 62, 46, 14);
		panel.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("E-mail");
		lblNewLabel_1_1_1.setBounds(62, 110, 46, 14);
		panel.add(lblNewLabel_1_1_1);
		
		txtDia = new JTextField();
		txtDia.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(txtDia.getText().length() >= 2)
					e.consume();
				    Validar.SoloNumeros(e);
			}
		});
		txtDia.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtDia.setBounds(59, 329, 32, 32);
		panel.add(txtDia);
		txtDia.setColumns(10);
		
		JLabel lblClubDeFutbol = new JLabel("Seleccione club de futbol:");
		lblClubDeFutbol.setBounds(20, 452, 189, 14);
		panel.add(lblClubDeFutbol);
		

		
		JSeparator separator_1_1_1_1 = new JSeparator();
		separator_1_1_1_1.setBounds(10, 530, 308, 7);
		panel.add(separator_1_1_1_1);

		//PAISES
		Vector<PaisDTO> pais = new Vector<PaisDTO>();
		pais.addAll(PaisDAOSQL.readAll());
		
		DefaultComboBoxModel modeloPais = new DefaultComboBoxModel(pais);
		CB_Pais.setModel(modeloPais);
		
		lblClub = new JLabel("");
		lblClub.setFont(new Font("Tahoma", Font.PLAIN, 21));
		lblClub.setBounds(113, 468, 196, 32);
		panel.add(lblClub);
		
		lblLiga = new JLabel("");
		lblLiga.setHorizontalAlignment(SwingConstants.TRAILING);
		lblLiga.setBounds(144, 505, 166, 14);
		panel.add(lblLiga);
				
		CB_Pais.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) 
            {
        		PaisDTO _pais = (dto.PaisDTO) CB_Pais.getSelectedItem();
        		setPais(_pais);
        		ComboProvincias(panel, evt ,_pais.getId());
        		ComboLocalidades(evt, 0, panel);
            }
        });
		
		if(accion == "Agregar")
			inicializarAgregar(panel);
		else if (accion == "Editar") 
			inicializarEditar(panel, persona, domicilio);
		
		
		this.setVisible(false);
		this.setVisible(false);
	}
	
	//PROVINCIAS
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void ComboProvincias(JPanel panel, java.awt.event.ItemEvent e, int id) {
	
	
	if(e.getStateChange() == ItemEvent.SELECTED) {
		Vector<ProvinciaDTO> provincia = new Vector<ProvinciaDTO>();
		provincia.add(null);
		provincia.addAll(ProvinciaDAOSQL.readAll(id));
		
			
		DefaultComboBoxModel modeloLocalidad = new DefaultComboBoxModel(provincia);
		CB_Provincia.setModel(modeloLocalidad);
    		
	}
	
	CB_Provincia.addItemListener(new java.awt.event.ItemListener() {
        public void itemStateChanged(java.awt.event.ItemEvent evt) 
        {
        	ProvinciaDTO provincia = (dto.ProvinciaDTO) CB_Provincia.getSelectedItem();
     		setProvincia(provincia);
            ComboLocalidades(evt, provincia.getId(), panel);		
        }
    });
	

	}
	
	@SuppressWarnings("unchecked")
	private void ComboLocalidades(java.awt.event.ItemEvent e, int id, JPanel panel) 
    {
		try {
			if(e.getStateChange() == ItemEvent.SELECTED) {
				Vector<LocalidadDTO> localidades = new Vector<LocalidadDTO>();
				localidades.add(null);
				localidades.addAll(LocalidadesDAOSQL.readAll(id));
			
				@SuppressWarnings("rawtypes")
				DefaultComboBoxModel modeloLocalidad = new DefaultComboBoxModel(localidades);
				CB_Localidad.setModel(modeloLocalidad);
				
			}
			
			CB_Localidad.addItemListener(new java.awt.event.ItemListener() {
	            public void itemStateChanged(java.awt.event.ItemEvent evt) 
	            {
	        		LocalidadDTO local = (dto.LocalidadDTO) CB_Localidad.getSelectedItem();
	       			setLocalidad(local);
	            }
	        });
		}catch (Exception e1) {
			CB_Localidad.setSelectedItem("");
			System.out.println(e1);
		}

	}


	
	/////////////////////////////////////////AGREGAR////////////////////////////////////////////////////////////
	private void inicializarAgregar(JPanel panel)
	{	
		this.setTitle("Agregar Persona");
		btnAgregarPersona = new JButton("");
		btnAgregarPersona.setBackground(Color.WHITE);
		btnAgregarPersona.setBounds(130, 535, 50, 50);
		cambiarIconoBotones(btnAgregarPersona, "add abm.png");
		btnAgregarPersona.addActionListener(this.controlador);
		btnAgregarPersona.setToolTipText("Agregar");
		
		panel.add(btnAgregarPersona);
		btnAgregarClub_persona = new JButton("");
		btnAgregarClub_persona.setBackground(new Color(255, 255, 224));
		btnAgregarClub_persona.setBounds(57,477,32,32);
		cambiarIconoBotones(btnAgregarClub_persona, "club.png");
		btnAgregarClub_persona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VistaClubes abmclubes = new VistaClubes("Agregar", null, null);
				abmclubes.show();
			}
		});
		panel.add(btnAgregarClub_persona);

		JButton btnAyuda = new JButton("");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ayuda ayuda = new Ayuda(ConstanteAyuda.VENTANAPERSONA_AGREGAR);
				ayuda.show();
			}
		});
		btnAyuda.setToolTipText("Ayuda");
		btnAyuda.setBounds(293, 565, 24, 25);
		cambiarIconoBotones(btnAyuda, "ayuda.png");
		btnAyuda.setToolTipText("Ayuda");
		panel.add(btnAyuda);
		
	}
	
	//////////////////////////////////////////EDITAR////////////////////////////////////////////////////////////
	private void inicializarEditar(JPanel panel, PersonaDTO persona, DomicilioDTO domicilio) {
		this.setTitle("Editar Persona");
		
		setIdPersona(persona.getIdPersona());
		txtNombre.setText(persona.getNombre());
		txtTelefono.setText(persona.getTelefono());
		txtCorreo.setText(persona.getCorreo());
		etiqueta = persona.get_etiqueta();
		mes.setMonth(persona.getMes()-1);
		txtCalle.setText(domicilio.getCalle());
		CamposIntegerNoRegistrados(persona, domicilio);
		VentanaPersona.lblClub.setText(persona.getClub());
		VentanaPersona.lblLiga.setText(persona.getLiga());
		
		try {
			PaisDTO __pais;
			int IndexProv;
			ProvinciaDTO __prov;
			int IndexLoc;
			__pais = ConseguirPais(domicilio);
			IndexProv = Conseguir_INDEX_Provincia(domicilio, __pais);
			__prov = ConseguirProvincia(domicilio, __pais);	
			IndexLoc = Conseguir_INDEX_localidad(domicilio, __prov);
			CB_Pais.setSelectedItem(__pais);
			CB_Provincia.setSelectedIndex(IndexProv);	
			CB_Localidad.setSelectedIndex(IndexLoc);
		} catch (Exception e) {
			CB_Pais.setSelectedItem("");
			CB_Provincia.setSelectedItem("");	
			CB_Localidad.setSelectedItem("");
		}
		
		try {
			TipoContactoDTO nuevaetiqueta = ConseguirEtiqueta(persona);
			CB_Etiquetas.setSelectedItem(nuevaetiqueta);
		} catch (Exception e) {
			CB_Etiquetas.setSelectedItem("");
		}
		btnEditarClub_persona = new JButton("");
		btnEditarClub_persona.setToolTipText("Editar");
		btnEditarClub_persona.setBackground(new Color(255, 255, 224));
		btnEditarClub_persona.setBounds(57,477,32,32);
		cambiarIconoBotones(btnEditarClub_persona, "club.png");
		btnEditarClub_persona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VistaClubes abmclubes = new VistaClubes("Editar", persona.getClub(), persona.getLiga());
				abmclubes.show();
			}
		});
		panel.add(btnEditarClub_persona);

		btnEditarPersona = new JButton("");
		btnEditarPersona.addActionListener(this.controlador);
		btnEditarPersona.setBounds(130, 535, 50, 50);
		cambiarIconoBotones(btnEditarPersona, "edit.png");
		btnEditarPersona.setToolTipText("Editar");
		panel.add(btnEditarPersona);
		
		JButton btnAyuda = new JButton("");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ayuda ayuda = new Ayuda(ConstanteAyuda.VENTANAPERSONA_EDITAR);
				ayuda.show();
			}
		});
		btnAyuda.setToolTipText("Ayuda");
		btnAyuda.setBounds(293, 565, 24, 25);
		cambiarIconoBotones(btnAyuda, "ayuda.png");
		btnAyuda.setToolTipText("Ayuda");
		panel.add(btnAyuda);
	}

	private void CamposIntegerNoRegistrados(PersonaDTO persona, DomicilioDTO domicilio) {
		if(persona.getDia() >= 1 ){
			txtDia.setText(""+persona.getDia()); 
		}else { txtDia.setText(""); }
		
		if(domicilio.getAltura() >= 1 ){
			txtAltura.setText(""+domicilio.getAltura());
		}else { txtAltura.setText(""); }
		
		if(domicilio.getPiso() >= 1 ){
			txtPiso.setText(""+domicilio.getPiso());
		}else { txtPiso.setText(""); }
		
		if(domicilio.getDepto() >= 1 ){
			txtDepto.setText(""+domicilio.getDepto());
		}else { txtDepto.setText(""); }
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int Conseguir_INDEX_localidad(DomicilioDTO domicilio, ProvinciaDTO __prov) {
		int __loc = 0;
		Vector<LocalidadDTO> localidades = new Vector<LocalidadDTO>();
		localidades.addAll(LocalidadesDAOSQL.readAll(__prov.getId()));
		for (LocalidadDTO local : localidades) {
			__loc ++;
			if(local.getNombre().equals(domicilio.getLocalidad()))
				return __loc; 
		}
		return __loc;
		
	}
	
	public ProvinciaDTO ConseguirProvincia(DomicilioDTO domicilio, PaisDTO __pais) {
		ProvinciaDTO __prov = null;
		Vector<ProvinciaDTO> provincias = new Vector<ProvinciaDTO>();
		provincias.addAll(ProvinciaDAOSQL.readAll(__pais.getId()));
		for (ProvinciaDTO provincia : provincias) {
			if(provincia.getNombre().equals(domicilio.getProvincia()))
				__prov = provincia;
		}
		return __prov;
	}


	public int Conseguir_INDEX_Provincia(DomicilioDTO domicilio, PaisDTO __pais) {
		int __prov = 0;
		Vector<ProvinciaDTO> provincias = new Vector<ProvinciaDTO>();
		provincias.addAll(ProvinciaDAOSQL.readAll(__pais.getId()));
		for (ProvinciaDTO provincia : provincias) {
			__prov ++;
			if(provincia.getNombre().equals(domicilio.getProvincia()))
				return __prov; 
		}
		return __prov;
		
	}


	public PaisDTO ConseguirPais(DomicilioDTO domicilio) {
		PaisDTO __pais = null;
		Vector<PaisDTO> paises = new Vector<PaisDTO>();
		paises.addAll(PaisDAOSQL.readAll());
		for (PaisDTO pais : paises) {
			if(pais.getNombre().equals(domicilio.getPais()))
				__pais = pais; 
		}
		return __pais;
	}


	public TipoContactoDTO ConseguirEtiqueta(PersonaDTO persona) {
		TipoContactoDTO nuevaetiqueta = null;
		Vector<TipoContactoDTO> etiquetas = new Vector<TipoContactoDTO>();
		etiquetas.addAll(TipoContactoDAOSQL.readAll());
		for (TipoContactoDTO etiqueta_ : etiquetas) {
			if(etiqueta_.getNombre().equals(persona.get_etiqueta()))
				nuevaetiqueta = etiqueta_; 
		}
		return nuevaetiqueta;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void setIdPersona(int idpersona) 
	{
		this.idpersona = idpersona;
	}
	public PersonaDTO getDatosPersona()
	{

		String club = lblClub.getText();
		String liga = lblLiga.getText();
		
		PersonaDTO persona = new PersonaDTO
				(idpersona,this.getTxtNombre().getText(), this.getTxtTelefono().getText(),this.getTxtEtiqueta(),  
						new DomicilioDTO(  idpersona,
										this.getTxtCalle().getText(),
										this.getTxtAltura(),
										this.getTxtPiso(), 
										this.getTxtDepto(),
										nombreLocalidad(),
										nombreProvincia(),
										nombrePais()					
					), this.getTxtCorreo(), this.getDia(), this.getMes(), club, liga);

		persona.setDia(getDia());
		persona.setMes(getMes());
		return persona;
	}
	
	public DomicilioDTO getDatosDomicilio() 
	{
		
		DomicilioDTO _Domicilio = new DomicilioDTO(  idpersona,
										this.getTxtCalle().getText(),
										this.getTxtAltura(),
										this.getTxtPiso(), 
										this.getTxtDepto(),
										nombreLocalidad(),
										nombreProvincia(),
										nombrePais()
										);
		return _Domicilio;
		
	}
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}
	
	public JButton getBtnEditarPersona() {
		return btnEditarPersona;
	}
	
	public JButton getBtnAgregarClub_persona() {
		return btnAgregarClub_persona;
	}

	public void setBtnAgregarClub_persona(JButton btnAgregarClub_persona) {
		this.btnAgregarClub_persona = btnAgregarClub_persona;
	}

	public JButton getBtnEditarClub_persona() {
		return btnEditarClub_persona;
	}

	public void setBtnEditarClub_persona(JButton btnEditarClub_persona) {
		this.btnEditarClub_persona = btnEditarClub_persona;
	}

	public String getTxtEtiqueta()
	{
		return etiqueta;
	}
	
	public TipoContactoDTO get_etiqueta() {
		return _etiqueta;
	}
	
	public void set_etiqueta(TipoContactoDTO _etiqueta) {
		this._etiqueta = _etiqueta;
	}
	
	
	public LocalidadDTO getLocalidad() {
		return localidad;
	}
	
	public String nombreLocalidad() {
		try {
			return getLocalidad().getNombre();		
		} catch (Exception e) {
			return "";
		}
	}

	public void setLocalidad(LocalidadDTO localidad) {
		this.localidad = localidad;
	}

	public ProvinciaDTO getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaDTO provincia) {
		this.provincia = provincia;
	}
	
	public String nombreProvincia() {
		try {
			return getProvincia().getNombre();		
		} catch (Exception e) {
			return "";
		}
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}
	

	
	public String nombrePais() {
		try {
			return getPais().getNombre();		
		} catch (Exception e) {
			return "";
		}
	}
	
	public JTextField getTxtCalle() 
	{
		return txtCalle;
	}
	
	public int getTxtAltura() 
	{
		try {
			return Integer.parseInt(txtAltura.getText());
		} catch (Exception e) { return 0; }
	}
	
	public JTextField getTxtAlt() {
		return txtAltura;
	}
	
	public int getTxtPiso() 
	{
		try {
			return Integer.parseInt(txtPiso.getText());
		} catch (Exception e) { return 0; }
	}
	
	public String getTxtPi() {
		return txtPiso.getText();
	}
	
	public int getTxtDepto() 
	{
		try {
			return Integer.parseInt(txtDepto.getText());
		} catch (Exception e) { return 0; }
	}
		
	public String getTxtDpto() {
		return txtDepto.getText();
	}
	
	public String getTxtCorreo() {
		return txtCorreo.getText();
	}
	
	public int getDia() {
		try {
			return Integer.parseInt(txtDia.getText());
		} catch (Exception e) { return 0; }
	}
	
	public int getMes() {
		return mes.getMonth()+1;
	}
	
	public ClubDTO getClubAddPersona() {
		return clubAddPersona;
	}
	
	public void setClubAddPersona(ClubDTO c) {
		this.clubAddPersona = c;
	}
	
	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.dispose();
	}
	
	public void crearImagen(JLabel label, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
	
	public void cambiarIconoBotones(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}
	
	@SuppressWarnings("unchecked")
	public static void ActualizarEtiquetas() {
		Vector<TipoContactoDTO> _etiquetas = new Vector<TipoContactoDTO>();
		_etiquetas.addAll(TipoContactoDAOSQL.readAll());
	
		@SuppressWarnings({ "rawtypes" })
		DefaultComboBoxModel modeloEtiqueta = new DefaultComboBoxModel(_etiquetas);
		CB_Etiquetas.setModel(modeloEtiqueta);
	}
	
	@SuppressWarnings("unchecked")
	public static void ActualizarPaises() {
		Vector<PaisDTO> _paises = new Vector<PaisDTO>();
		_paises.addAll(PaisDAOSQL.readAll());
	
		@SuppressWarnings({ "rawtypes" })
		DefaultComboBoxModel modeloPaises = new DefaultComboBoxModel(_paises);
		CB_Pais.setModel(modeloPaises);
	}
}

