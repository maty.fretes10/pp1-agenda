package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import dto.PersonaDTO;

import javax.swing.JButton;

import persistencia.conexion.Conexion;
import java.awt.event.ActionListener;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;


public class Vista
{
	private JFrame frmAgenda;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnAyuda;
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"CONTACTO", "TELEFONO", "CORREO", "LOCALIDAD" ,"DOMICILIO", "ETIQUETA", "CUMPLEA�OS", "CLUB", "LIGA"};

	public Vista() 
	{
		super();
		initialize();
	}

	private void initialize() 
	{
		try {
			  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception e) {
			  System.out.println("Error setting native LAF: " + e);
		}
		frmAgenda = new JFrame();
		frmAgenda.setResizable(false);
		frmAgenda.setTitle("Agenda");
		frmAgenda.getContentPane().setBackground(Color.WHITE);
		frmAgenda.setBounds(100, 100, 799, 352);
		frmAgenda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgenda.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(220, 220, 220));
		panel.setBounds(0, 0, 793, 323);
		frmAgenda.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBackground(new Color(245, 255, 250));
		spPersonas.setBounds(66, 32, 703, 255);
		spPersonas.setEnabled(false);
		panel.add(spPersonas);


		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		

		btnAgregar = new JButton();
		btnAgregar.setBounds(10, 32, 46, 47);
		btnAgregar.setForeground(Color.BLACK);
		btnAgregar.setBackground(Color.WHITE);
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		cambiarIcono(btnAgregar, "AgregarContacto.png");
		btnAgregar.setToolTipText("Agregar");
		panel.add(btnAgregar);
		
		btnEditar = new JButton();
		btnEditar.setToolTipText("Editar");
		btnEditar.setBounds(10, 90, 46, 47);
		btnEditar.setBackground(Color.WHITE);
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		cambiarIcono(btnEditar, "EditarContacto.png");
		panel.add(btnEditar);
		
		btnBorrar = new JButton("");
		btnBorrar.setBounds(10, 148, 46, 47);
		btnBorrar.setBackground(Color.WHITE);
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		cambiarIcono(btnBorrar, "EliminarContacto.png");
		btnBorrar.setToolTipText("Eliminar");
		panel.add(btnBorrar);
		
		btnReporte = new JButton("");
		btnReporte.setBounds(10, 206, 46, 47);
		btnReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnReporte.setBackground(Color.WHITE);
		cambiarIcono(btnReporte, "IconReporte.png");
		btnReporte.setToolTipText("Reporte");
		panel.add(btnReporte);
		
		btnAyuda = new JButton("");
		btnAyuda.setBounds(736, 287, 24, 25);
		cambiarIcono(btnAyuda, "ayuda.png");
		btnAyuda.setToolTipText("Ayuda");
		panel.add(btnAyuda);
	}
	
	public void show()
	{
		this.frmAgenda.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgenda.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "�Estas seguro que quieres salir de la Agenda?", 
		             "Confirmacion", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frmAgenda.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnEditar()
	{
		return btnEditar;
	}
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public JButton getBtnAyuda() {
		return btnAyuda;
	}

	public void setBtnAyuda(JButton btnAyuda) {
		this.btnAyuda = btnAyuda;
	}

	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public void cambiarIcono(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}


	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String correo = p.getCorreo();
			String localidad;
			if(p.getDom().getLocalidad().length() == 0) {
				localidad = "";
			}else { localidad = p.getDom().getLocalidad(); }
			
			String domicilio;
			if (p.getDom().getAltura() == 0) {
				domicilio = p.getDom().getCalle();
			}else { domicilio = p.getDom().getCalle() + " " + p.getDom().getAltura();}
			
			String etiqueta = p.get_etiqueta();
			if (etiqueta.equals("Sin etiqueta")){
				etiqueta = "";
			}
			
			String cumple;
			if(p.getDia() == 0) {
				cumple = "";
			}else {cumple =  p.getDia() + " / " + p.getMes();}
			
			
			String club;
			if(p.getClub().length() == 0) {
				club = "";
			}else { club = p.getClub(); } 
			
			String liga;
			if(p.getLiga().length() == 0) {
				liga = "";
			}else { liga = p.getLiga(); } 
			 
			Object[] fila = {nombre, tel, correo, localidad, domicilio, etiqueta, cumple, club, liga};
			this.getModelPersonas().addRow(fila);
		}
		
	}
}
