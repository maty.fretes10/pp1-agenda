package presentacion.vista;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JTextField;
import javax.swing.UIManager;
import modelo.Agenda;
import persistencia.conexion.Conexion;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.Controlador;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ConexionMySQL {

	private JFrame frmAgenda;
	private static JTextField txtUser;
	private static JPasswordField txtContra;
	private static boolean EstadoConexion;
	private static JTextField txtIP;
	private static JTextField txtPuerto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConexionMySQL window = new ConexionMySQL();
					window.frmAgenda.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ConexionMySQL() {
		initialize();
	}

	public void show()
	{
		this.frmAgenda.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgenda.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "�Estas seguro que quieres salir de la Agenda?", 
		             "Confirmacion", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {

		           System.exit(0);
		        }
		    }
		});
		this.frmAgenda.setVisible(true);
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception e) {
			  System.out.println("Error setting native LAF: " + e);
		}
		frmAgenda = new JFrame();
		frmAgenda.setResizable(false);
		frmAgenda.setTitle("Agenda");
		frmAgenda.setBounds(100, 100, 325, 243);
		frmAgenda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgenda.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("CONEXION CON LA BASE DE DATOS");
		lblNewLabel.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel.setBounds(61, 11, 224, 36);
		frmAgenda.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("USUARIO");
		lblNewLabel_1.setFont(new Font("Arial Narrow", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(54, 58, 50, 14);
		frmAgenda.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("CONTRASE\u00D1A");
		lblNewLabel_1_1.setFont(new Font("Arial Narrow", Font.PLAIN, 11));
		lblNewLabel_1_1.setBounds(54, 93, 74, 14);
		frmAgenda.getContentPane().add(lblNewLabel_1_1);
		
		txtUser = new JTextField();
		txtUser.setBounds(129, 55, 156, 20);
		frmAgenda.getContentPane().add(txtUser);
		txtUser.setColumns(10);
		
		txtContra = new JPasswordField();
		txtContra.setBounds(129, 90, 156, 20);
		frmAgenda.getContentPane().add(txtContra);
		txtContra.setColumns(10);
		
		txtIP = new JTextField();
		txtIP.setBounds(18, 137, 110, 20);
		frmAgenda.getContentPane().add(txtIP);
		txtIP.setColumns(10);
		
		txtPuerto = new JTextField();
		txtPuerto.setBounds(146, 137, 55, 20);
		frmAgenda.getContentPane().add(txtPuerto);
		txtPuerto.setColumns(10);
		
		JLabel mjsError = new JLabel("* Campos incorrectos *");
		mjsError.setForeground(Color.RED);
		mjsError.setBounds(14, 168, 199, 14);
		frmAgenda.getContentPane().add(mjsError);
		mjsError.setVisible(false);
		
		JLabel mjsVacio = new JLabel("* Todos los campos son obligatorios *");
		mjsVacio.setForeground(Color.RED);
		mjsVacio.setBounds(10, 168, 199, 14);
		frmAgenda.getContentPane().add(mjsVacio);
		mjsVacio.setVisible(false);
		
		
		JButton btnValidar = new JButton("");
		
		btnValidar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mjsError.setVisible(false);
				
				if(txtUser.getText().length() == 0 || String.valueOf(txtContra.getPassword()).length() == 0 || txtIP.getText().length() == 0 || txtPuerto.getText().length() == 0) 
				{
					mjsVacio.setVisible(true);
				}else {
					Conexion.getConexion();
					
					if(!EstadoConexion) {
						mjsVacio.setVisible(false);
						mjsError.setVisible(true);
						Conexion.instancia = null;
					}else {
						Vista vista = new Vista();
						Agenda modelo = new Agenda(new DAOSQLFactory());
						Controlador controlador = new Controlador(vista, modelo);
						controlador.inicializar();
						frmAgenda.setVisible(false);
					}
				}
								
			}
		});
		btnValidar.setBounds(223, 125, 74, 66);
		frmAgenda.getContentPane().add(btnValidar);
		cambiarIconoBotones(btnValidar, "enter.png");
		
		JLabel icnUser = new JLabel("");
		icnUser.setBounds(10, 47, 28, 28);
		frmAgenda.getContentPane().add(icnUser);
		crearImagen(icnUser, "user.png");
		
		JLabel icnPass = new JLabel("");
		icnPass.setBounds(10, 86, 28, 28);
		frmAgenda.getContentPane().add(icnPass);
		crearImagen(icnPass, "pass.png");
		
		JLabel lblNewLabel_2 = new JLabel("DIRECCION IP");
		lblNewLabel_2.setFont(new Font("Arial Narrow", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(21, 125, 107, 14);
		frmAgenda.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("PUERTO");
		lblNewLabel_2_1.setFont(new Font("Arial Narrow", Font.PLAIN, 11));
		lblNewLabel_2_1.setBounds(147, 125, 107, 14);
		frmAgenda.getContentPane().add(lblNewLabel_2_1);
		
	}



	public static String getTxtUser() 
	{
		return txtUser.getText();
	}


	public static String getTxtContra() {
		return String.valueOf(txtContra.getPassword());
	}
	
	

	public static void setTxtUser(JTextField txtUser) {
		ConexionMySQL.txtUser = txtUser;
	}

	public static void setTxtContra(JPasswordField txtContra) {
		ConexionMySQL.txtContra = txtContra;
	}
	
	public static String getTxtIP() {
		return txtIP.getText();
	}

	public static void setTxtIP(JTextField txtIP) {
		ConexionMySQL.txtIP = txtIP;
	}

	public static String getTxtPuerto() {
		return txtPuerto.getText();
	}

	public static void setTxtPuerto(JTextField txtPuerto) {
		ConexionMySQL.txtPuerto = txtPuerto;
	}

	public static void ConexionExitosa() {
		
		JOptionPane.showMessageDialog(null, "Conexion Exitosa");
	}

	public static boolean isEstadoConexion() {
		return EstadoConexion;
	}

	public static void setEstadoConexion(boolean estadoConexion) {
		EstadoConexion = estadoConexion;
	}
	public void crearImagen(JLabel label, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
	
	public void cambiarIconoBotones(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}
}
