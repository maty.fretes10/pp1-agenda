package presentacion.vista.ABMlocalidades;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import dto.PaisDTO;
import dto.LocalidadDTO;
import dto.ProvinciaDTO;
import persistencia.dao.mysql.LocalidadesDAOSQL;
import persistencia.dao.mysql.PaisDAOSQL;
import persistencia.dao.mysql.ProvinciaDAOSQL;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class VentanaBorrarLocalidades {

	private JFrame frmEliminar;
	private PaisDTO pais;
	private ProvinciaDTO provincia;
	private LocalidadDTO localidad;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxPais;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxProvincia;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxLocalidad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaBorrarLocalidades window = new VentanaBorrarLocalidades();
					window.frmEliminar.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaBorrarLocalidades() {
		initialize();
	}
	
	public void show()
	{
		this.frmEliminar.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		frmEliminar = new JFrame();
		frmEliminar.setTitle("Eliminar");
		frmEliminar.setBounds(100, 100, 359, 272);
		frmEliminar.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Pais:");
		lblNewLabel.setBounds(31, 52, 89, 14);
		frmEliminar.getContentPane().add(lblNewLabel);
		
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(31, 87, 89, 14);
		frmEliminar.getContentPane().add(lblProvincia);
		
		JLabel lblLocalidad = new JLabel("Localidad:");
		lblLocalidad.setBounds(31, 123, 102, 14);
		frmEliminar.getContentPane().add(lblLocalidad);
		
		
		JButton btnCerrar = new JButton("CERRAR");
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmEliminar.dispose();
			}
		});
		btnCerrar.setBounds(45, 186, 89, 23);
		frmEliminar.getContentPane().add(btnCerrar);
		
		comboBoxPais = new JComboBox();
		comboBoxPais.setBounds(130, 48, 163, 22);
		frmEliminar.getContentPane().add(comboBoxPais);
		
		comboBoxProvincia = new JComboBox();
		comboBoxProvincia.setBounds(130, 83, 163, 22);
		frmEliminar.getContentPane().add(comboBoxProvincia);
		
		comboBoxLocalidad = new JComboBox();
		comboBoxLocalidad.setBounds(130, 119, 163, 22);
		frmEliminar.getContentPane().add(comboBoxLocalidad);
		
		
		Vector<PaisDTO> pais = new Vector<PaisDTO>();
		pais.addAll(PaisDAOSQL.readAll());
		
		DefaultComboBoxModel modeloPais = new DefaultComboBoxModel(pais);
		comboBoxPais.setModel(modeloPais);
		setPais((dto.PaisDTO) comboBoxPais.getSelectedItem());

		comboBoxPais.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) 
            {
        		PaisDTO _pais = (dto.PaisDTO) comboBoxPais.getSelectedItem();
        		ComboProvincias(evt ,_pais.getId(), comboBoxProvincia);

            }
        });
		
		JButton btnEliminar = new JButton("ELIMINAR");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PaisDTO _pais = (dto.PaisDTO) comboBoxPais.getSelectedItem();		
				ProvinciaDTO _prov = (dto.ProvinciaDTO) comboBoxProvincia.getSelectedItem();
				LocalidadDTO _loc = (dto.LocalidadDTO) comboBoxLocalidad.getSelectedItem();
				
				if (_pais==null||_prov==null||_loc==null) {
					JOptionPane.showMessageDialog(null, "Todos los campos deben tener un valor seleccionado");
				}else {
					logicaEliminarLocalidades(_pais, _prov, _loc);
					frmEliminar.dispose();
				}

			}

			private void logicaEliminarLocalidades(PaisDTO _pais, ProvinciaDTO _prov, LocalidadDTO _loc) {
				if(_pais.getNombre().length() > 0) 										 
				{
					if(_prov == null && _loc == null) 
					{ 
						new PaisDAOSQL().delete(_pais);
					}
					
					else if(_prov != null && _loc == null)  
					{
						new ProvinciaDAOSQL().delete(_prov);
					}
					
					else if(_prov != null && _loc != null)  
					{
						new LocalidadesDAOSQL().delete(_loc);
					}
				}
				
				else 
				{
					JOptionPane.showMessageDialog(null, "Ingrese pais a eliminar");
				}
			}

	
		});
		btnEliminar.setBounds(144, 186, 89, 23);
		frmEliminar.getContentPane().add(btnEliminar);
		
		JLabel ayuda1 = new JLabel("Debe ingresar un pais, una provincia y una localidad");
		ayuda1.setForeground(new Color(0, 0, 255));
		ayuda1.setBounds(41, 148, 323, 14);
		frmEliminar.getContentPane().add(ayuda1);
		ayuda1.setVisible(false);
		
		JLabel ayuda2 = new JLabel("Solo se elimina la localidad");
		ayuda2.setForeground(Color.BLUE);
		ayuda2.setBounds(99, 161, 275, 14);
		frmEliminar.getContentPane().add(ayuda2);
		ayuda2.setVisible(false);

		
		JButton btnAbrirayuda = new JButton("");
		JButton btnCerrarayuda = new JButton("");
		
		btnCerrarayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayuda1.setVisible(false);
				ayuda2.setVisible(false);
				btnCerrarayuda.setVisible(false);
				btnAbrirayuda.setVisible(true);
			}
		});
		btnCerrarayuda.setBounds(310, 186, 23, 23);
		frmEliminar.getContentPane().add(btnCerrarayuda);
		btnCerrarayuda.setVisible(false);
		
		

		btnAbrirayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayuda1.setVisible(true);
				ayuda2.setVisible(true);
				btnAbrirayuda.setVisible(false);
				btnCerrarayuda.setVisible(true);
			}
		});
		btnAbrirayuda.setBounds(310, 186, 23, 23);
		frmEliminar.getContentPane().add(btnAbrirayuda);
		
		cambiarIconoBotones(btnAbrirayuda,"ayuda.png");
		cambiarIconoBotones(btnCerrarayuda,"ayuda.png");

	}
	
	public void cambiarIconoBotones(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}
	
	
	
		@SuppressWarnings({ "rawtypes", "unchecked" })
	private void ComboProvincias(java.awt.event.ItemEvent e, int id, JComboBox comboBox_prov) {
		
		
		if(e.getStateChange() == ItemEvent.SELECTED) {
			Vector<ProvinciaDTO> provincia = new Vector<ProvinciaDTO>();
			provincia.add(null);
			provincia.addAll(ProvinciaDAOSQL.readAll(id));
			
				
			DefaultComboBoxModel modeloLocalidad = new DefaultComboBoxModel(provincia);
			comboBox_prov.setModel(modeloLocalidad);
			
			setProvincia((dto.ProvinciaDTO) comboBox_prov.getSelectedItem());
	
		}
		
		comboBox_prov.addItemListener(new java.awt.event.ItemListener() {
	        public void itemStateChanged(java.awt.event.ItemEvent evt) 
	        {
	    		ProvinciaDTO provincia = (dto.ProvinciaDTO) comboBox_prov.getSelectedItem();
	        	cambiolocalidades(evt, provincia.getId());
	        }
	    });
	
	}

	@SuppressWarnings("unchecked")
	private void cambiolocalidades(java.awt.event.ItemEvent e, int id) 
	{
		
		if(e.getStateChange() == ItemEvent.SELECTED) {
			Vector<LocalidadDTO> localidades = new Vector<LocalidadDTO>();
			localidades.add(null);
			localidades.addAll(LocalidadesDAOSQL.readAll(id));
			
				
			@SuppressWarnings("rawtypes")
			DefaultComboBoxModel modeloLocalidad = new DefaultComboBoxModel(localidades);
			comboBoxLocalidad.setModel(modeloLocalidad);
			
			setLocalidad((dto.LocalidadDTO) comboBoxLocalidad.getSelectedItem());
	
	
		}
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}

	public ProvinciaDTO getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaDTO provincia) {
		this.provincia = provincia;
	}

	public LocalidadDTO getLocalidad() {
		return localidad;
	}

	public void setLocalidad(LocalidadDTO localidad) {
		this.localidad = localidad;
	} 
	
	
}
