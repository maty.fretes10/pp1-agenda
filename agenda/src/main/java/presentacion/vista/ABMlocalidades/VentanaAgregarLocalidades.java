package presentacion.vista.ABMlocalidades;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;

import dto.PaisDTO;
import dto.LocalidadDTO;
import dto.ProvinciaDTO;
import persistencia.dao.mysql.LocalidadesDAOSQL;
import persistencia.dao.mysql.PaisDAOSQL;
import persistencia.dao.mysql.ProvinciaDAOSQL;

import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.Color;


public class VentanaAgregarLocalidades {

	private JFrame frmAgregarLocalidad;
	private JTextField txtPais;
	private JTextField txtProvincia;
	private JTextField txtLocalidad;
	private int idpais;
	private int idprov;
	private int idloc;
	private PaisDTO paisRepe;
	private ProvinciaDTO provRepe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaAgregarLocalidades window = new VentanaAgregarLocalidades();
					window.frmAgregarLocalidad.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaAgregarLocalidades() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	public void show()
	{
		this.frmAgregarLocalidad.setVisible(true);
	}
	
	private void initialize() {
		frmAgregarLocalidad = new JFrame();
		frmAgregarLocalidad.setTitle("Agregar Localidad");
		frmAgregarLocalidad.setBounds(100, 100, 299, 281);
		frmAgregarLocalidad.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Pais");
		lblNewLabel.setBounds(10, 14, 122, 34);
		frmAgregarLocalidad.getContentPane().add(lblNewLabel);
		
		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(10, 61, 122, 34);
		frmAgregarLocalidad.getContentPane().add(lblProvincia);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 115, 122, 34);
		frmAgregarLocalidad.getContentPane().add(lblLocalidad);
		
				
		txtPais = new JTextField();
		txtPais.setBounds(82, 21, 153, 20);
		frmAgregarLocalidad.getContentPane().add(txtPais);
		txtPais.setColumns(10);
		
		txtProvincia = new JTextField();
		txtProvincia.setColumns(10);
		txtProvincia.setBounds(82, 68, 153, 20);
		frmAgregarLocalidad.getContentPane().add(txtProvincia);
		
		txtLocalidad = new JTextField();
		txtLocalidad.setColumns(10);
		txtLocalidad.setBounds(82, 122, 153, 20);
		frmAgregarLocalidad.getContentPane().add(txtLocalidad);
		
		JButton btnNewButton = new JButton("Agregar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (txtPais.getText().isBlank()||txtProvincia.getText().isBlank()||txtLocalidad.getText().isBlank()){
					mostrarMensaje();
				}else {
					Vector<PaisDTO> pais = new Vector<PaisDTO>();
					pais.addAll(PaisDAOSQL.readAll());
					
					boolean pais_repetido = false;
					
					for (PaisDTO pais2 : pais) {
						if(pais2.getNombre().equals(txtPais.getText())) {
							pais_repetido = pais_repetido || true;	
							paisRepe = pais2;
						}
					}
					
					if(pais_repetido)
						crearDesdePaisRepetido();
					else
						if(txtPais.getText().length()>0)
							CrearDesdePaisNuevo();	
					frmAgregarLocalidad.dispose();
				}
				
				
			}

			private void mostrarMensaje() {
				String mensaje="";
				
				if (txtPais.getText().isBlank()) {
					mensaje += "Debe ingresar un pais \n";
				}
				if (txtProvincia.getText().isBlank()) {
					mensaje += "Debe ingresar una provincia \n";
 				}
				if (txtLocalidad.getText().isBlank()) {
					mensaje += "Debe ingresar una localidad \n";
				}

				JOptionPane.showMessageDialog(null, mensaje);
			}
		});
		btnNewButton.setBounds(138, 208, 89, 23);
		frmAgregarLocalidad.getContentPane().add(btnNewButton);
		
		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	
				frmAgregarLocalidad.dispose();
			}
		});
		btnCerrar.setBounds(25, 208, 89, 23);
		frmAgregarLocalidad.getContentPane().add(btnCerrar);
		
		
		JLabel ayudapais = new JLabel("Debe ingresar el nombre del pais");
		ayudapais.setForeground(new Color(0, 0, 255));
		ayudapais.setBounds(73, 43, 200, 14);
		frmAgregarLocalidad.getContentPane().add(ayudapais);
		ayudapais.setVisible(false);
		
		JLabel ayudaprovincia = new JLabel("Luego de ingresar del pais, ingresar provincia");
		ayudaprovincia.setForeground(Color.BLUE);
		ayudaprovincia.setBounds(48, 90, 253, 14);
		frmAgregarLocalidad.getContentPane().add(ayudaprovincia);
		ayudaprovincia.setVisible(false);
		
		JLabel ayudalocalidad = new JLabel("Luego de ingresar del provincia, ingresar localidad");
		ayudalocalidad.setForeground(Color.BLUE);
		ayudalocalidad.setBounds(20, 143, 253, 14);
		frmAgregarLocalidad.getContentPane().add(ayudalocalidad);
		ayudalocalidad.setVisible(false);
		
		JButton btnAbrirayuda = new JButton("");
		JButton btnCerrarayuda = new JButton("");
		
		btnCerrarayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayudapais.setVisible(false);
				ayudaprovincia.setVisible(false);
				ayudalocalidad.setVisible(false);
				btnCerrarayuda.setVisible(false);
				btnAbrirayuda.setVisible(true);
			}
		});
		btnCerrarayuda.setBounds(250, 208, 23, 23);
		frmAgregarLocalidad.getContentPane().add(btnCerrarayuda);
		btnCerrarayuda.setVisible(false);
		
		

		btnAbrirayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayudapais.setVisible(true);
				ayudaprovincia.setVisible(true);
				ayudalocalidad.setVisible(true);
				btnAbrirayuda.setVisible(false);
				btnCerrarayuda.setVisible(true);
			}
		});
		btnAbrirayuda.setBounds(250, 208, 23, 23);
		frmAgregarLocalidad.getContentPane().add(btnAbrirayuda);
		
		cambiarIconoBotones(btnAbrirayuda,"ayuda.png");
		cambiarIconoBotones(btnCerrarayuda,"ayuda.png");

	}
	
	public void cambiarIconoBotones(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}

	public void CrearDesdePaisNuevo() {

			PaisDTO pais_nuevo = new PaisDTO(idpais, txtPais.getText());
			new PaisDAOSQL().insert(pais_nuevo);
			
			Vector<PaisDTO> nuevosPaises = new Vector<PaisDTO>();
			nuevosPaises.addAll(PaisDAOSQL.readAll());
			
			for (PaisDTO pais : nuevosPaises) {
				if(pais.getNombre().equals(txtPais.getText())) {
					idpais = pais.getId();
				}
			}
			
			if(txtProvincia.getText().length() > 0)	
			{
				new ProvinciaDAOSQL().insert(new ProvinciaDTO( idpais, idprov, txtProvincia.getText()));
				Vector<ProvinciaDTO> nuevosProvincias = new Vector<ProvinciaDTO>();
				nuevosProvincias.add(null);
				nuevosProvincias.addAll(ProvinciaDAOSQL.readAll(idpais));
				
				for (ProvinciaDTO provincia : nuevosProvincias) {
					if(provincia != null) {	
						if(provincia.getNombre().equals(txtProvincia.getText())) {
							idprov = provincia.getId();
						}
					}
				}
				if( txtLocalidad.getText().length() > 0)	
				{
			new LocalidadesDAOSQL().insert(new LocalidadDTO(idloc, idprov, txtLocalidad.getText()));
				}
			}
			
			if(txtProvincia.getText().length() == 0 && txtLocalidad.getText().length() > 0) 
			{
				JOptionPane.showMessageDialog(null,"CREAR PROVINCIA ANTES QUE LA LOCALIDAD");
			}

	}
	
	public void crearDesdePaisRepetido() {

		if (paisRepe.getNombre().length() > 0) 	//SI EL PAIS ES REPETIDO
		{
			Vector <ProvinciaDTO> _provincia = new Vector <ProvinciaDTO>();
			_provincia.addAll(ProvinciaDAOSQL.readAll(paisRepe.getId()));
			
			boolean prov_repetida = false;
			
			if(txtProvincia.getText().length() > 0) {
				for (ProvinciaDTO provincia2 : _provincia) {
					if(provincia2.getNombre().equals(txtProvincia.getText())) {
						prov_repetida = prov_repetida || true;
						provRepe = provincia2;
					}
				}
				
				if(!prov_repetida) {		//SI NO EXISTE PROVINCIA SE CREA, Y SI TIENE LOCALIDAD TAMBIEN
					ProvinciaDTO provNueva= new ProvinciaDTO(paisRepe.getId(), idprov, txtProvincia.getText());
					new ProvinciaDAOSQL().insert(provNueva);
		
					Vector<ProvinciaDTO> nuevosProvincias = new Vector<ProvinciaDTO>();
					nuevosProvincias.add(null);
					nuevosProvincias.addAll(ProvinciaDAOSQL.readAll(paisRepe.getId()));
					
					for (ProvinciaDTO provincia : nuevosProvincias) {
						if(provincia != null) {	
							if(provincia.getNombre().equals(txtProvincia.getText())) {
								idprov = provincia.getId();
							}
						}
					}
					
					if(txtLocalidad.getText().length()>0) 
					{
						LocalidadDTO localNuevo = new LocalidadDTO(idloc, idprov,txtLocalidad.getText());
						new LocalidadesDAOSQL().insert(localNuevo);
					}
					
				}
				
				else if(prov_repetida && txtLocalidad.getText().length()>0) {	//provincia repetida y localidad completada
					Vector <LocalidadDTO> _local = new Vector <LocalidadDTO>();
					_local.addAll(LocalidadesDAOSQL.readAll(provRepe.getId()));
					boolean loc_repetida = false;	
					
					for (LocalidadDTO local2 : _local) {
						if(local2.getNombre().equals(txtLocalidad.getText())) {
							loc_repetida = loc_repetida || true;
							JOptionPane.showMessageDialog(null, "LOCALIDAD REPETIDA: "+ txtLocalidad.getText());
						}
					}
									
					
					if(!loc_repetida) {
					
						LocalidadDTO localNuevo = new LocalidadDTO(idloc, provRepe.getId(),txtLocalidad.getText());
						new LocalidadesDAOSQL().insert(localNuevo);						
					}
				
				}
			}
			
			if(txtProvincia.getText().length() == 0 && txtLocalidad.getText().length() > 0) 
			{
				JOptionPane.showMessageDialog(null,"CREAR PROVINCIA ANTES QUE LA LOCALIDAD");
			}
			
		}
	}
}
