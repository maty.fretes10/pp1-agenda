package presentacion.vista.ABMlocalidades;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import presentacion.vista.VentanaPersona;

import javax.swing.JLabel;

public class VistaABMLocalidades 
{
	private JFrame frame;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private DefaultTableModel modelLocalidades;
	private  String[] nombreColumnas = {"Descripcion"};
	private JLabel lblNewLabel;

	public VistaABMLocalidades() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 277, 300);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 261, 262);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		modelLocalidades = new DefaultTableModel(null,nombreColumnas);

		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaAgregarLocalidades agregar_local = new VentanaAgregarLocalidades();
				agregar_local.show();
			}
		});
		btnAgregar.setBounds(58, 46, 155, 40);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaEditarLocalidades editar_local = new VentanaEditarLocalidades();
				editar_local.show();
			}
		});
		btnEditar.setBounds(58, 97, 155, 40);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaBorrarLocalidades borrar_local = new VentanaBorrarLocalidades();
				borrar_local.show();
			}
		});
		btnBorrar.setBounds(58, 148, 155, 40);
		panel.add(btnBorrar);
		
		lblNewLabel = new JLabel("Que desea hacer con las localidades?");
		lblNewLabel.setBounds(20, 22, 300, 14);
		panel.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Cerrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaPersona.ActualizarPaises();
				frame.dispose();
			}
		});
		btnNewButton.setBounds(124, 228, 89, 23);
		panel.add(btnNewButton);
		
		frame.setTitle("ABM Localidades");
	}
	
	public void show()
	{
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar()
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public DefaultTableModel getModelLocalidades() 
	{
		return modelLocalidades;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
}

