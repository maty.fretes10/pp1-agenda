package presentacion.vista.ABMlocalidades;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import dto.PaisDTO;
import dto.LocalidadDTO;
import dto.ProvinciaDTO;
import persistencia.dao.mysql.LocalidadesDAOSQL;
import persistencia.dao.mysql.PaisDAOSQL;
import persistencia.dao.mysql.ProvinciaDAOSQL;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class VentanaEditarLocalidades {

	private JFrame frmEditarLocalidades;
	private JTextField txtNuevoPais;
	private JTextField txtNuevaProv;
	private JTextField txtNuevaLoc;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox_Pais;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox_Provincia;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox_Localidad;
	private PaisDTO pais;
	private ProvinciaDTO provincia;
	private LocalidadDTO localidad;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEditarLocalidades window = new VentanaEditarLocalidades();
					window.frmEditarLocalidades.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaEditarLocalidades() {
		initialize();
	}
	
	public void show()
	{
		this.frmEditarLocalidades.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		frmEditarLocalidades = new JFrame();
		frmEditarLocalidades.setTitle("Editar Localidades");
		frmEditarLocalidades.setBounds(100, 100, 362, 359);
		frmEditarLocalidades.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Pais:");
		lblNewLabel.setBounds(10, 40, 110, 14);
		frmEditarLocalidades.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nuevo pais:");
		lblNewLabel_1.setBounds(10, 65, 110, 14);
		frmEditarLocalidades.getContentPane().add(lblNewLabel_1);
		
		comboBox_Pais = new JComboBox();
		comboBox_Pais.setBounds(159, 36, 152, 22);
		frmEditarLocalidades.getContentPane().add(comboBox_Pais);
		
		txtNuevoPais = new JTextField();
		txtNuevoPais.setBounds(159, 62, 152, 20);
		frmEditarLocalidades.getContentPane().add(txtNuevoPais);
		txtNuevoPais.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Provincia");
		lblNewLabel_2.setBounds(10, 130, 110, 14);
		frmEditarLocalidades.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("Nueva provincia");
		lblNewLabel_2_1.setBounds(10, 155, 110, 14);
		frmEditarLocalidades.getContentPane().add(lblNewLabel_2_1);
		
		comboBox_Provincia = new JComboBox();
		comboBox_Provincia.setBounds(159, 126, 152, 22);
		frmEditarLocalidades.getContentPane().add(comboBox_Provincia);
		
		txtNuevaProv = new JTextField();
		txtNuevaProv.setColumns(10);
		txtNuevaProv.setBounds(159, 152, 152, 20);
		frmEditarLocalidades.getContentPane().add(txtNuevaProv);
		
		JLabel lblNewLabel_2_2 = new JLabel("Localidad");
		lblNewLabel_2_2.setBounds(10, 215, 110, 14);
		frmEditarLocalidades.getContentPane().add(lblNewLabel_2_2);
		
		JLabel lblNewLabel_2_1_1 = new JLabel("Nueva localidad");
		lblNewLabel_2_1_1.setBounds(10, 240, 110, 14);
		frmEditarLocalidades.getContentPane().add(lblNewLabel_2_1_1);
		
		comboBox_Localidad = new JComboBox();
		comboBox_Localidad.setBounds(159, 211, 152, 22);
		frmEditarLocalidades.getContentPane().add(comboBox_Localidad);
		
		txtNuevaLoc = new JTextField();
		txtNuevaLoc.setColumns(10);
		txtNuevaLoc.setBounds(159, 237, 152, 20);
		frmEditarLocalidades.getContentPane().add(txtNuevaLoc);
		

		
		JButton btnNewButton = new JButton("MODIFICAR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (txtNuevoPais.getText().isBlank()||txtNuevaProv.getText().isBlank()||txtNuevaLoc.getText().isBlank()){
					mostrarMensaje();
				}else {
					try {
						PaisDTO _pais = modificarPais();
						ProvinciaDTO _prov = modificarProvincia(_pais);
						modificarLocalidad(_prov);
					}catch (NullPointerException ee) {
						JOptionPane.showMessageDialog(null, "Todos los campos deben tener un valor seleccionado");
					}
					frmEditarLocalidades.dispose();
				}

			}

			private PaisDTO modificarPais() {
				//INGRESAR UN PAIS PARA EDITAR
				PaisDTO _pais = (dto.PaisDTO) comboBox_Pais.getSelectedItem();
				if(_pais.getNombre().length() > 0) {
					if(txtNuevoPais.getText().length()>0) {
						//PONER VALOR DEL TEXT EN EL COMBO BOX
						_pais.setNombre(txtNuevoPais.getText());
						new PaisDAOSQL().update(_pais);
					}
				}

				return _pais;
			}

			private ProvinciaDTO modificarProvincia(PaisDTO _pais) {
				//INGRESAR UNA PROVINCIA PARA EDITAR
				ProvinciaDTO _prov = (dto.ProvinciaDTO) comboBox_Provincia.getSelectedItem();		
				if(txtNuevaProv.getText().length()>0) {
					//PONER VALOR DEL TEXT EN EL COMBO BOX
					_prov.setIdpais(_pais.getId());
					_prov.setNombre(txtNuevaProv.getText());
					new ProvinciaDAOSQL().update(_prov);
				}
				
				return _prov;
			}

			private void modificarLocalidad(ProvinciaDTO _prov) {
				//INGRESAR UNA LOCALIDAD PARA EDITAR
				LocalidadDTO _localidad = (dto.LocalidadDTO) comboBox_Localidad.getSelectedItem();		
				if(txtNuevaLoc.getText().length()>0) {
					//PONER VALOR DEL TEXT EN EL COMBO BOX
					_localidad.setId_prov(_prov.getId());
					_localidad.setNombre(txtNuevaLoc.getText());
										
					new LocalidadesDAOSQL().update(_localidad);		
				}
			}
			private void mostrarMensaje() {
				String mensaje="";
				
				if (txtNuevoPais.getText().isBlank()) {
					mensaje += "Debe ingresar un pais \n";
				}
				if (txtNuevaProv.getText().isBlank()) {
					mensaje += "Debe ingresar una provincia \n";
 				}
				if (txtNuevaLoc.getText().isBlank()) {
					mensaje += "Debe ingresar una localidad \n";
				}
				
				JOptionPane.showMessageDialog(null, mensaje);
			}
		});
		
		
		btnNewButton.setBounds(159, 286, 110, 23);
		frmEditarLocalidades.getContentPane().add(btnNewButton);
		
		JButton btnCerrar = new JButton("CERRAR");
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmEditarLocalidades.dispose();
			}
		});
		btnCerrar.setBounds(21, 286, 110, 23);
		frmEditarLocalidades.getContentPane().add(btnCerrar);
		
		Vector<PaisDTO> pais = new Vector<PaisDTO>();
		pais.addAll(PaisDAOSQL.readAll());
		
		DefaultComboBoxModel modeloPais = new DefaultComboBoxModel(pais);
		comboBox_Pais.setModel(modeloPais);
		
		
		JLabel ayudapais = new JLabel("Debe seleccionar un pais y a continuacion puede editarlo");
		ayudapais.setForeground(new Color(0, 0, 255));
		ayudapais.setBounds(10, 15, 326, 14);
		frmEditarLocalidades.getContentPane().add(ayudapais);
		ayudapais.setVisible(false);
		
		JLabel ayudaprovincia = new JLabel("Debe seleccionar una provincia y a continuacion puede editarlo");
		ayudaprovincia.setForeground(Color.BLUE);
		ayudaprovincia.setBounds(10, 105, 326, 14);
		frmEditarLocalidades.getContentPane().add(ayudaprovincia);
		ayudaprovincia.setVisible(false);

		
		JLabel ayudalocalidad = new JLabel("Debe seleccionar una localidad y a continuacion puede editarlo");
		ayudalocalidad.setForeground(Color.BLUE);
		ayudalocalidad.setBounds(10, 190, 326, 14);
		frmEditarLocalidades.getContentPane().add(ayudalocalidad);
		setPais((dto.PaisDTO) comboBox_Pais.getSelectedItem());
		ayudalocalidad.setVisible(false);

		JButton btnAbrirayuda = new JButton("");
		JButton btnCerrarayuda = new JButton("");
		
		btnCerrarayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayudapais.setVisible(false);
				ayudaprovincia.setVisible(false);
				ayudalocalidad.setVisible(false);
				btnCerrarayuda.setVisible(false);
				btnAbrirayuda.setVisible(true);
			}
		});
		btnCerrarayuda.setBounds(313, 286, 23, 23);
		frmEditarLocalidades.getContentPane().add(btnCerrarayuda);
		btnCerrarayuda.setVisible(false);
		
		

		btnAbrirayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayudapais.setVisible(true);
				ayudaprovincia.setVisible(true);
				ayudalocalidad.setVisible(true);
				btnAbrirayuda.setVisible(false);
				btnCerrarayuda.setVisible(true);
			}
		});
		btnAbrirayuda.setBounds(313, 286, 23, 23);
		frmEditarLocalidades.getContentPane().add(btnAbrirayuda);
		
		cambiarIconoBotones(btnAbrirayuda,"ayuda.png");
		cambiarIconoBotones(btnCerrarayuda,"ayuda.png");
		
		
		comboBox_Pais.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) 
            {
        		PaisDTO _pais = (dto.PaisDTO) comboBox_Pais.getSelectedItem();
        		txtNuevoPais.setText(_pais.getNombre());
        		ComboProvincias(evt ,_pais.getId(), comboBox_Provincia);


            }
        });
		
	}
	
		@SuppressWarnings({ "rawtypes", "unchecked" })
	private void ComboProvincias(java.awt.event.ItemEvent e, int id, JComboBox comboBox_prov) {
		
		
		if(e.getStateChange() == ItemEvent.SELECTED) {
			Vector<ProvinciaDTO> provincia = new Vector<ProvinciaDTO>();
			provincia.add(null);
			provincia.addAll(ProvinciaDAOSQL.readAll(id));	
			
			DefaultComboBoxModel modeloLocalidad = new DefaultComboBoxModel(provincia);
			comboBox_prov.setModel(modeloLocalidad);
			
			setProvincia((dto.ProvinciaDTO) comboBox_prov.getSelectedItem());
	
		}
		
		comboBox_prov.addItemListener(new java.awt.event.ItemListener() {
	        public void itemStateChanged(java.awt.event.ItemEvent evt) 
	        {
	    		ProvinciaDTO provincia = (dto.ProvinciaDTO) comboBox_prov.getSelectedItem();
	    		txtNuevaProv.setText(provincia.getNombre());
	    		cambiolocalidades(evt, provincia.getId());
	        	
	        }
	    });
	
	}

	@SuppressWarnings("unchecked")
	private void cambiolocalidades(java.awt.event.ItemEvent e, int id) 
	{
		
		if(e.getStateChange() == ItemEvent.SELECTED) {
			Vector<LocalidadDTO> localidades = new Vector<LocalidadDTO>();
			localidades.add(null);
			localidades.addAll(LocalidadesDAOSQL.readAll(id));
				
			@SuppressWarnings("rawtypes")
			DefaultComboBoxModel modeloLocalidad = new DefaultComboBoxModel(localidades);
			comboBox_Localidad.setModel(modeloLocalidad);
			
		}
		
		comboBox_Localidad.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) 
            {
    			LocalidadDTO local = (dto.LocalidadDTO) comboBox_Localidad.getSelectedItem();
    			txtNuevaLoc.setText(local.getNombre());
    			setLocalidad(local);
            }
        });
		
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}

	public ProvinciaDTO getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaDTO provincia) {
		this.provincia = provincia;
	}

	public LocalidadDTO getLocalidad() {
		return localidad;
	}

	public void setLocalidad(LocalidadDTO localidad) {
		this.localidad = localidad;
	}
	
	public void cambiarIconoBotones(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}

}
