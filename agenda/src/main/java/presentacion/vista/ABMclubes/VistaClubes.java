package presentacion.vista.ABMclubes;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import dto.ClubDTO;
import dto.LigaDTO;
import persistencia.dao.mysql.ClubDAOSQL;
import persistencia.dao.mysql.LigaDAOSQL;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Ayuda.Ayuda;
import presentacion.vista.Ayuda.ConstanteAyuda;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class VistaClubes implements ActionListener
{
	private JFrame frmEditarClubes;
	private JPanel panel;
	@SuppressWarnings("rawtypes")
	private JComboBox CB_Liga;
	@SuppressWarnings("rawtypes")
	private JComboBox CB_Club;
	private static LigaDTO liga;
	private static ClubDTO club;
	private JTextField txtLiga;
	private JTextField txtClub;
	private JButton btnEditarClub;
	private JButton btnAgregarClub;
	private JButton btnBorrarClub;
	private static int idLiga;
	private static int idClub;
	private JButton btnActualizar;
	private JButton btnABMAgregar; 
	private JButton btnABMEditar;
	private String mensaje;
	private JLabel lblMensaje;
	private JLabel lblNewLabel;
	private JLabel lblClub;
	private JLabel lblNuevaLiga;
	private JLabel lblNuevoClub;

	public VistaClubes(String accion, String clubPersona , String ligaClubPersona) 
	{
		super();
		initialize(accion, clubPersona, ligaClubPersona);
	}

	private void inicializarAgregar() {
	
	}
	
	private void inicializarEditar(String clubPersona, String ligaClubPersona) {
		
		CB_Liga.setSelectedItem(ligaClubPersona);
		CB_Club.setSelectedItem(clubPersona);
	}

	@SuppressWarnings({ "rawtypes"})
	private void initialize(String accion, String clubPersona, String ligaClubPersona) 
	{
		frmEditarClubes = new JFrame();
		frmEditarClubes.setResizable(false);
		frmEditarClubes.setBounds(100, 100, 372, 429);
		frmEditarClubes.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 366, 400);
		frmEditarClubes.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblNuevaLiga = new JLabel("NUEVA LIGA\r\n");
		lblNuevaLiga.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNuevaLiga.setForeground(Color.WHITE);
		lblNuevaLiga.setBounds(18, 190, 77, 14);
		panel.add(lblNuevaLiga);
		lblNuevaLiga.setVisible(false);
		
		lblNuevoClub = new JLabel("NUEVO CLUB\r\n");
		lblNuevoClub.setForeground(Color.WHITE);
		lblNuevoClub.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNuevoClub.setBounds(20, 278, 77, 14);
		panel.add(lblNuevoClub);
		lblNuevoClub.setVisible(false);
		
		btnABMEditar = new JButton("");
		btnABMEditar.setToolTipText("Editar");
		btnABMEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LigaDTO _liga = modificarLiga();
				ClubDTO _club = modificarClub(_liga);
				txtLiga.setText("");
				txtClub.setText("");
		
				actualizarLigasClubes();

				CB_Liga.setSelectedItem(_liga);
				CB_Club.setSelectedItem(_club);
			}
		});
		btnABMEditar.setBounds(315, 269, 23, 23);
		panel.add(btnABMEditar);
		btnABMEditar.setVisible(false);
		cambiarIconoBotones(btnABMEditar, "refresh.png");
		
		btnABMAgregar = new JButton("");
		btnABMAgregar.setToolTipText("Agregar");
		btnABMAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chequeoAgregar();
				txtLiga.setText("");
				txtClub.setText("");
				CB_Club.setSelectedItem("");
				CB_Liga.setSelectedItem("");
				actualizarLigasClubes();
			}
		});
		btnABMAgregar.setBounds(315, 269, 23, 23);
		panel.add(btnABMAgregar);
		btnABMAgregar.setVisible(false);
		cambiarIconoBotones(btnABMAgregar, "add.png");
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(148, 11, 70, 70);
		crearImagen(lblNewLabel_1, "soccer_ball.png");
		panel.add(lblNewLabel_1);
		
		lblMensaje = new JLabel("");
		lblMensaje.setBackground(Color.WHITE);
		lblMensaje.setForeground(Color.WHITE);
		lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblMensaje.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMensaje.setBounds(20, 303, 322, 24);
		lblMensaje.setVisible(false);
		panel.add(lblMensaje);
		
		txtLiga = new JTextField();
		txtLiga.setColumns(10);
		txtLiga.setBounds(105, 184, 199, 27);
		panel.add(txtLiga);

		txtClub = new JTextField();
		txtClub.setColumns(10);
		txtClub.setBounds(105, 267, 199, 27);
		panel.add(txtClub);

		txtLiga.setVisible(false);
		txtClub.setVisible(false);
		
		CB_Liga = new JComboBox();
		CB_Liga.setBackground(Color.WHITE);
		CB_Liga.setFont(new Font("Tahoma", Font.PLAIN, 21));
		CB_Liga.setBounds(64, 140, 240, 40);
		panel.add(CB_Liga);
		
		CB_Club = new JComboBox();
		CB_Club.setBackground(Color.WHITE);
		CB_Club.setFont(new Font("Tahoma", Font.PLAIN, 21));
		CB_Club.setBounds(64, 222, 240, 40);
		panel.add(CB_Club);
		
		JButton btnAyuda = new JButton("");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ayuda ayuda = new Ayuda(ConstanteAyuda.VISTACLUBES);
				ayuda.show();
			}
		});
		btnAyuda.setToolTipText("Ayuda");
		btnAyuda.setBounds(315, 11, 23, 23);
		cambiarIconoBotones(btnAyuda, "ayuda.png");
		panel.add(btnAyuda);
		
		btnEditarClub = new JButton("");
		btnEditarClub.setToolTipText("Editar Club");
		btnEditarClub.setForeground(Color.WHITE);
		btnEditarClub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(CB_Liga.getSelectedItem() != null && CB_Club.getSelectedItem() != null) {
					txtLiga.setVisible(true);
					txtLiga.setText(getLiga().getNombre());
					txtClub.setVisible(true);
					txtClub.setText(getClub().getNombreClub());
					btnABMEditar.setVisible(true);
					btnABMAgregar.setVisible(false);
					lblMensaje.setVisible(false);
					lblNuevaLiga.setVisible(true);
					lblNuevoClub.setVisible(true);
				}
				else {
					lblMensaje.setText("Seleccionar liga y club a editar");
					lblMensaje.setVisible(true);
				}

			}
		});
		btnEditarClub.setBackground(Color.WHITE);
		btnEditarClub.setBounds(64, 338, 35, 35);
		cambiarIconoBotones(btnEditarClub, "edit_ball.png");
		panel.add(btnEditarClub);
		
		btnAgregarClub = new JButton("");
		btnAgregarClub.setToolTipText("Agregar Club");
		btnAgregarClub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnABMEditar.setVisible(false);
				btnABMAgregar.setVisible(true);
				txtLiga.setVisible(true);
				txtClub.setVisible(true);
				txtLiga.setText("");
				txtClub.setText("");
				lblNuevaLiga.setVisible(true);
				lblNuevoClub.setVisible(true);
			}
		});
		btnAgregarClub.setBackground(Color.WHITE);
		btnAgregarClub.setBounds(20, 338, 35, 35);
		cambiarIconoBotones(btnAgregarClub, "add_ball.png");
		panel.add(btnAgregarClub);
		
		btnBorrarClub = new JButton("");
		btnBorrarClub.setToolTipText("Borrar Club");
		btnBorrarClub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (CB_Club.getSelectedItem() != null) {
					ClubDTO club_a_borrar = (dto.ClubDTO) CB_Club.getSelectedItem();
			        int confirm = JOptionPane.showOptionDialog(
				             null, "�Estas seguro que quieres borrar el club " + '"' + club_a_borrar.getNombreClub()+ '"' + " ?", 
				             "Advertencia", JOptionPane.YES_NO_OPTION,
				             JOptionPane.WARNING_MESSAGE, null, null, null);
				        if (confirm == 0) {
							borrarClub(club_a_borrar);
							actualizarClubes(club_a_borrar.getIdLiga());
							
							lblMensaje.setText("Club " + '"' + club_a_borrar.getNombreClub()+ '"' + " eliminado");
							lblMensaje.setVisible(true);
				        }

				}
				else {
					lblMensaje.setText("Seleccionar club a eliminar");
					lblMensaje.setVisible(true);
				
				}
			}
		});
		btnBorrarClub.setBackground(Color.WHITE);
		btnBorrarClub.setBounds(109, 338, 35, 35);
		cambiarIconoBotones(btnBorrarClub, "delete_ball.png");
		panel.add(btnBorrarClub);
		
	
		btnActualizar = new JButton("");
		btnActualizar.setToolTipText("Elegir");
		btnActualizar.setBackground(Color.WHITE);
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  	
            	chequeoElegirClub();
			}

		});
		btnActualizar.setBounds(281, 338, 35, 35);
		cambiarIconoBotones(btnActualizar, "save_ball.png");
		panel.add(btnActualizar);
		
		actualizarLigasClubes();

		if(accion == "Agregar")
			inicializarAgregar();
		else if (accion == "Editar") 
			inicializarEditar(clubPersona, ligaClubPersona);
		frmEditarClubes.setTitle("EDITAR CLUBES");
		
		JLabel lblNewLabel_2 = new JLabel("Equipo de f\u00FAtbol");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Arial Narrow", Font.BOLD, 31));
		lblNewLabel_2.setBackground(Color.WHITE);
		lblNewLabel_2.setBounds(79, 81, 209, 40);
		panel.add(lblNewLabel_2);
			
		
		lblNewLabel = new JLabel("LIGA: ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(8, 157, 46, 14);
		panel.add(lblNewLabel);
		
		lblClub = new JLabel("CLUB:");
		lblClub.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblClub.setForeground(Color.WHITE);
		lblClub.setBounds(8, 239, 46, 14);
		panel.add(lblClub);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(0, 0, 366, 400);
		crearImagen(lblNewLabel_3, "pasto.png");
		panel.add(lblNewLabel_3);
			
	}
	
	
	public void show()
	{
		this.frmEditarClubes.setVisible(true);
	}
	
	public void cerrar()
	{
		this.frmEditarClubes.setVisible(false);
	}
	
	public void cambiarIconoBotones(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}
	
	public void crearImagen(JLabel label, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
	
	@SuppressWarnings("static-access")
	public void setLiga(LigaDTO liga) {
		this.liga = liga;
	}
	
	public static LigaDTO getLiga() {
		return liga;
	}

	public void setClub(ClubDTO club) {
		VistaClubes.club = club;
	}
	public static ClubDTO getClub() {
		return club;
	}
	
	public LigaDTO obtenerLiga(ClubDTO club) {
		Vector<LigaDTO> ligas = new Vector<LigaDTO>();
		ligas.addAll(LigaDAOSQL.readAll());
		LigaDTO nuevaLiga = null;
		for(LigaDTO liga : ligas) {
			if(liga.getIdLiga()==club.getIdLiga()) {
				nuevaLiga = liga;
			}
		}
		return nuevaLiga;
	}

	public JButton getBtnEditarClub() {
		return btnEditarClub;
	}


	public JButton getBtnAgregarClub() {
		return btnAgregarClub;
	}


	public JButton getBtnBorrarClub() {
		return btnBorrarClub;
	}

	public static int getIdLiga()
	{
		try { return idLiga;
		} catch (Exception e) { return 0; }
	}
	
	public static int getIdClub() 
	{
		try { return idClub;
		} catch (Exception e) { return 0; }	
	}
	
	private int LigaExistente(String nombreLiga) {
		new LigaDAOSQL();
		Vector<LigaDTO> ligasexistentes = LigaDAOSQL.readAll();
		int ret = -1;
		
		for (LigaDTO ligaDTO : ligasexistentes) {
			if(ligaDTO.getNombre().equals(nombreLiga)) {
				ret = ligaDTO.getIdLiga();
			}
		}
		return ret;
	}
	
	private boolean clubExistenteEnLiga(String nombreClub, int liga) {
		new ClubDAOSQL();
		Vector<ClubDTO> clubexistente = ClubDAOSQL.readAll(liga);
		boolean ret = false;
		
		for (ClubDTO clubDTO : clubexistente) {
			if (clubDTO.getNombreClub().equals(nombreClub)) {
				return true;
			}
		}
		return ret;
	}
	
	private void chequeoElegirClub() {	
		try {
			VentanaPersona.lblClub.setText(CB_Club.getSelectedItem().toString());
			VentanaPersona.lblLiga.setText(CB_Liga.getSelectedItem().toString());
			frmEditarClubes.dispose();
		} catch (Exception e) {
			VentanaPersona.lblClub.setText("");
			VentanaPersona.lblLiga.setText("");
			frmEditarClubes.dispose();
		}

	}
	
	private ClubDTO modificarClub(LigaDTO _liga) {
		ClubDTO _club = (dto.ClubDTO) CB_Club.getSelectedItem();		
		if(txtClub.getText().length()>0) {
			_club.setIdLiga(_liga.getIdLiga());
			_club.setNombreClub(txtClub.getText());
			new ClubDAOSQL().update(_club);
		}
		return _club;
	}

	private LigaDTO modificarLiga() {
		LigaDTO _liga = (LigaDTO) CB_Liga.getSelectedItem();
		if(_liga.getNombre().length() > 0) {
			if(txtLiga.getText().length() > 0) {
				_liga.setNombre(txtLiga.getText());
				new LigaDAOSQL().update(_liga);
			}
		}
		return _liga;
	}
	
	private void borrarClub(ClubDTO _club) {
		new ClubDAOSQL().delete(_club);
	}
	
	private void chequeoAgregar() {
		// Si escribe solo una liga        
    	if(txtLiga.getText().length() > 0 && txtClub.getText().isBlank()) 
    	{
    		if(LigaExistente(txtLiga.getText()) < 0){
        		LigaDTO nuevaLiga = new LigaDTO(idLiga, txtLiga.getText());
        		new LigaDAOSQL().insert(nuevaLiga);
				VentanaPersona.lblClub.setText("");
				VentanaPersona.lblLiga.setText("");
							
				lblMensaje.setText("Nueva liga " + '"' + nuevaLiga.getNombre() + '"' + " creada");
				lblMensaje.setVisible(true);
    		}
    	}
    	
    	// Si escribe solo un club
    	if(txtLiga.getText().isBlank() && txtClub.getText().length() > 0) {
    		
    		LigaDTO l= (LigaDTO) CB_Liga.getSelectedItem();

    		int idLigaExistente = l.getIdLiga();
    		ClubDTO nuevoClub = new ClubDTO(idLigaExistente, idClub, txtClub.getText());
    		new ClubDAOSQL().insert(nuevoClub);
		
			lblMensaje.setText("Club " + '"' + txtClub.getText() + '"' + "a�adido");
			lblMensaje.setVisible(true);
    		
    	}
    	
    	// Si escribe los dos
		if(txtLiga.getText().length() > 0 && txtClub.getText().length() > 0) {
			int ligaexistente = LigaExistente(txtLiga.getText());

			//SI NO EXISTE LIGA
			if( ligaexistente < 0){
				int ligRecienCreada = 0;
				
        		LigaDTO nuevaLiga = new LigaDTO(idLiga, txtLiga.getText());
        		new LigaDAOSQL().insert(nuevaLiga);
        		  		
        		Vector<LigaDTO> ligas = LigaDAOSQL.readAll(); 
        		for (LigaDTO ligaDTO : ligas) {
					if(ligaDTO.getNombre().equals(txtLiga.getText())) {
						ligRecienCreada = ligaDTO.getIdLiga();
					}
				}
        		ClubDTO nuevoClub = new ClubDTO(ligRecienCreada , idClub, txtClub.getText());
        		new ClubDAOSQL().insert(nuevoClub);
    			lblMensaje.setText("Club " + '"' + txtClub.getText() + '"' + ", Liga: " + '"' + txtLiga.getText() + '"');
    			lblMensaje.setVisible(true);
        		
    		}
			//SI EXISTE LIGA
   			if (ligaexistente >= 0  && !clubExistenteEnLiga(txtClub.getText(), ligaexistente)) {
   				ClubDTO nuevoClub = new ClubDTO( ligaexistente, idClub, txtClub.getText());
        		new ClubDAOSQL().insert(nuevoClub);
        		
    			lblMensaje.setText("Club " + '"' + txtClub.getText() + '"');
    			lblMensaje.setVisible(true);
    			}
		}
		
		actualizarLigasClubes();
		
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void actualizarLigasClubes() {
		Vector<LigaDTO> ligas = new Vector<LigaDTO>();
		ligas.addAll(LigaDAOSQL.readAll());
		
		DefaultComboBoxModel modeloLiga = new DefaultComboBoxModel(ligas);
		CB_Liga.setModel(modeloLiga);
		CB_Liga.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) 
            {
        		LigaDTO _liga = (dto.LigaDTO) CB_Liga.getSelectedItem();
        		setLiga(_liga);
        		ComboClubes(evt ,_liga.getIdLiga(), panel);      
        		lblMensaje.setVisible(false);
            }
        });
	}

	private void ComboClubes(java.awt.event.ItemEvent e, int id, JPanel panel) 
    {
		try {
			if(e.getStateChange() == ItemEvent.SELECTED) {
				actualizarClubes(id);
			}
			
			CB_Club.addItemListener(new java.awt.event.ItemListener() {
	            public void itemStateChanged(java.awt.event.ItemEvent evt) 
	            {
	        		ClubDTO club = (dto.ClubDTO) CB_Club.getSelectedItem();
	       			setClub(club);
	            }
	        });
		}catch (Exception e1) {
			CB_Club.setSelectedItem("");
			System.out.println(e1);
		}

	}
	@SuppressWarnings("unchecked")
	private void actualizarClubes(int id) {
		Vector<ClubDTO> clubes = new Vector<ClubDTO>();
		clubes.add(null);
		clubes.addAll(ClubDAOSQL.readAll(id));
	
		@SuppressWarnings("rawtypes")
		DefaultComboBoxModel modeloClub = new DefaultComboBoxModel(clubes);
		CB_Club.setModel(modeloClub);
	}
	
	
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
}
