package presentacion.vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.TipoContactoDTO;
import persistencia.dao.mysql.TipoContactoDAOSQL;
import presentacion.vista.Ayuda.Ayuda;

import java.awt.Color;
import java.awt.Image;

public class VistaTiposContacto implements ActionListener
{
	private JFrame frmEditarEtiquetas;
	private JTable tablaTiposContacto;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private DefaultTableModel modelTiposContacto;
	private List<TipoContactoDTO> tiposContacto_en_tabla;
	private VentanaTiposContacto ventanaTiposContacto;
	private  String[] nombreColumnas = {"Descripcion"};
	private JButton btnGuardar;

	public VistaTiposContacto() 
	{
		super();
		initialize();
		this.getBtnAgregar().addActionListener(this);
		this.getBtnBorrar().addActionListener(this);
		this.getBtnEditar().addActionListener(this);
		llenarTabla();
	}


	private void initialize() 
	{
		frmEditarEtiquetas = new JFrame();
		frmEditarEtiquetas.setResizable(false);
		frmEditarEtiquetas.setBounds(100, 100, 369, 300);
		frmEditarEtiquetas.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 353, 262);
		frmEditarEtiquetas.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spTiposContacto = new JScrollPane();
		spTiposContacto.setBounds(10, 11, 259, 240);
		panel.add(spTiposContacto);
		
		modelTiposContacto = new DefaultTableModel(null,nombreColumnas);
		tablaTiposContacto = new JTable(modelTiposContacto);
		
		tablaTiposContacto.getColumnModel().getColumn(0).setPreferredWidth(130);
		tablaTiposContacto.getColumnModel().getColumn(0).setResizable(false);
		
		spTiposContacto.setViewportView(tablaTiposContacto);

		btnAgregar = new JButton("");
		btnAgregar.setBackground(Color.WHITE);
		btnAgregar.setBounds(294, 14, 40, 40);
		cambiarIconoBotones(btnAgregar, "add abm.png");
		btnAgregar.setToolTipText("Agregar");
		panel.add(btnAgregar);
		
		btnEditar = new JButton("");
		btnEditar.setBackground(Color.WHITE);
		btnEditar.setBounds(294, 65, 40, 40);
		cambiarIconoBotones(btnEditar, "edit.png");
		btnEditar.setToolTipText("Editar");
		panel.add(btnEditar);
		
		btnBorrar = new JButton("");
		btnBorrar.setBackground(Color.WHITE);
		btnBorrar.setBounds(294, 116, 40, 40);
		cambiarIconoBotones(btnBorrar, "delete.png");
		btnBorrar.setToolTipText("Eliminar");
		panel.add(btnBorrar);
		
		btnGuardar = new JButton("");
		btnGuardar.setBackground(Color.WHITE);
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaPersona.ActualizarEtiquetas();
				cerrar();
			}
		});
		btnGuardar.setBounds(294, 167, 40, 40);
		cambiarIconoBotones(btnGuardar, "save.png");
		btnGuardar.setToolTipText("Guardar");
		panel.add(btnGuardar);
		
		frmEditarEtiquetas.setTitle("EDITAR ETIQUETAS");
		
		JButton btnAyuda = new JButton("");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ayuda ayuda = new Ayuda("VistaTiposContacto");
				ayuda.show();
			}
		});
		btnAyuda.setToolTipText("Ayuda");
		btnAyuda.setBounds(319, 226, 24, 25);
		cambiarIconoBotones(btnAyuda, "ayuda.png");
		btnAyuda.setToolTipText("Ayuda");
		panel.add(btnAyuda);

		
		
	}
	
	public void show()
	{
		this.frmEditarEtiquetas.setVisible(true);
	}
	
	public void cerrar()
	{
		this.frmEditarEtiquetas.setVisible(false);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar()
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public DefaultTableModel getModelTiposContacto() 
	{
		return modelTiposContacto;
	}
	
	public JTable getTablaTiposContacto()
	{
		return tablaTiposContacto;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public void llenarTabla() 
	{
		this.getModelTiposContacto().setRowCount(0); //Para vaciar la tabla
		this.getModelTiposContacto().setColumnCount(0);
		this.getModelTiposContacto().setColumnIdentifiers(this.getNombreColumnas());

		new TipoContactoDAOSQL();
		this.tiposContacto_en_tabla = TipoContactoDAOSQL.readAll();
		for (int i = 0; i < this.tiposContacto_en_tabla.size(); i ++)
		{
			Object[] fila = 
			{
				this.tiposContacto_en_tabla.get(i).getNombre(), 
				
			};
			this.getModelTiposContacto().addRow(fila);
		}
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == this.getBtnAgregar())
		{
			this.ventanaTiposContacto = new VentanaTiposContacto(this,"Agregar",null);
		}
		else if(e.getSource() == this.getBtnEditar())
		{
			int[] filas_seleccionadas = this.getTablaTiposContacto().getSelectedRows();
			if(filas_seleccionadas.length == 1)
			{					
					TipoContactoDTO tipoContacto_a_obtener = new TipoContactoDAOSQL().getById(this.tiposContacto_en_tabla.get(filas_seleccionadas[0]));
					this.ventanaTiposContacto = new VentanaTiposContacto(this,"Editar",tipoContacto_a_obtener);
			}
		}
		else if(e.getSource() == this.getBtnBorrar())
		{
			int[] filas_seleccionadas = this.getTablaTiposContacto().getSelectedRows();
			for (int fila:filas_seleccionadas)
			{
				new TipoContactoDAOSQL().delete(this.tiposContacto_en_tabla.get(fila));
			}
				
			this.llenarTabla();

		}
		else if(e.getSource() == this.ventanaTiposContacto.getBtnAgregarTipoContacto())
		{
			TipoContactoDTO nuevoTipoContacto = this.ventanaTiposContacto.getDatosTipoContacto();
			new TipoContactoDAOSQL().insert(nuevoTipoContacto);
			this.llenarTabla();
			this.ventanaTiposContacto.dispose();
		}
		else if(e.getSource() == this.ventanaTiposContacto.getBtnEditarTipoContacto())
		{
			TipoContactoDTO editarTipoContacto = this.ventanaTiposContacto.getDatosTipoContacto();
			new TipoContactoDAOSQL().update(editarTipoContacto);
			this.llenarTabla();
			this.ventanaTiposContacto.dispose();
		}
		
	}
	
	public void cambiarIconoBotones(JButton boton, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		boton.setIcon(Icono);
	}
}
