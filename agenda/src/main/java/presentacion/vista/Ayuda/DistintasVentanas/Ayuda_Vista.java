package presentacion.vista.Ayuda.DistintasVentanas;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Ayuda_Vista 
{
	private JFrame frmAyuda;

	public Ayuda_Vista(JFrame frame) 
	{	
		frmAyuda = frame;
		frmAyuda.setTitle("Ayuda");
		frmAyuda.setBounds(100, 100, 450, 318);
		frmAyuda.getContentPane().setLayout(null);
		
		JLabel lblagregarContacto = new JLabel("");
		lblagregarContacto.setBounds(32, 11, 45, 45);
		crearImagen(lblagregarContacto, "AgregarContacto.png");
		frmAyuda.getContentPane().add(lblagregarContacto);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAyuda.dispose();
			}
		});
		btnNewButton.setBounds(187, 238, 47, 23);
		frmAyuda.getContentPane().add(btnNewButton);
		
		JLabel lbleditarContacto = new JLabel("");
		lbleditarContacto.setBounds(32, 67, 45, 45);
		crearImagen(lbleditarContacto, "EditarContacto.png");
		frmAyuda.getContentPane().add(lbleditarContacto);
		
		JLabel lbleliminarContacto = new JLabel("");
		lbleliminarContacto.setBounds(32, 123, 45, 45);
		crearImagen(lbleliminarContacto, "EliminarContacto.png");
		frmAyuda.getContentPane().add(lbleliminarContacto);
		
		JLabel lblreporteContacto = new JLabel("");
		lblreporteContacto.setBounds(32, 179, 45, 45);
		crearImagen(lblreporteContacto, "IconReporte.png");
		frmAyuda.getContentPane().add(lblreporteContacto);
		
		JLabel lblNewLabel = new JLabel("AGREGAR CONTACTO");
		lblNewLabel.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel.setBounds(87, 11, 147, 23);
		frmAyuda.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Abre una nueva ventana, permite ingresar datos del contacto.");
		lblNewLabel_1.setBounds(87, 33, 337, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1);
		
		JLabel lblEditarContacto = new JLabel("EDITAR CONTACTO");
		lblEditarContacto.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblEditarContacto.setBounds(87, 67, 147, 23);
		frmAyuda.getContentPane().add(lblEditarContacto);
		
		JLabel lblNewLabel_1_1 = new JLabel("Debe seleccionar un contacto de la lista y podra editarlo.");
		lblNewLabel_1_1.setBounds(87, 87, 337, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblEliminarContacto = new JLabel("ELIMINAR CONTACTO");
		lblEliminarContacto.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblEliminarContacto.setBounds(87, 128, 147, 23);
		frmAyuda.getContentPane().add(lblEliminarContacto);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Debe seleccionar un contacto de la lista y podra eliminarlo.");
		lblNewLabel_1_1_1.setBounds(87, 147, 337, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1);
		
		JLabel lblReporte = new JLabel("REPORTE");
		lblReporte.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblReporte.setBounds(87, 177, 147, 23);
		frmAyuda.getContentPane().add(lblReporte);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("Proporciona un reporte de contactos, agrupado y ordenados por");
		lblNewLabel_1_1_1_1.setBounds(87, 195, 337, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1);
		
		JLabel lblNewLabel_2 = new JLabel("etiquetas.");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setBounds(87, 207, 70, 14);
		frmAyuda.getContentPane().add(lblNewLabel_2);
	}
	
	public void crearImagen(JLabel label, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
}
