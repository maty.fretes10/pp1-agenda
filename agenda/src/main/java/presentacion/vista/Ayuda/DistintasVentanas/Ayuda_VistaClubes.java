package presentacion.vista.Ayuda.DistintasVentanas;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Ayuda_VistaClubes {

	private JFrame frmAyuda;

	public Ayuda_VistaClubes(JFrame frame) {
		frmAyuda = frame;
		frmAyuda.setTitle("Ayuda de vista de clubes");
		frmAyuda.setBounds(100, 100, 690, 556);
		frmAyuda.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 674, 517);
		frmAyuda.getContentPane().add(lblNewLabel);
		
		crearImagen(lblNewLabel, "ayuda_club.png");
	
	}

	public void crearImagen(JLabel label, String ruta) 
	{
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
}
