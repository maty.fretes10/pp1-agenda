package presentacion.vista.Ayuda.DistintasVentanas;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;

import presentacion.vista.Ayuda.ConstanteAyuda;

import java.awt.Font;


public class Ayuda_VentanaPersona {

	private JFrame frmAyuda;

	public Ayuda_VentanaPersona(JFrame frame, String accion) {
		
		frmAyuda = frame;
		
		if (accion.equals(ConstanteAyuda.VENTANAPERSONA_AGREGAR)) {
			frmAyuda.setTitle("Ayuda agregar persona");
			
			JLabel lblNewLabel_1_1_2_1_1_1_1_1_1 = new JLabel("CONFIRMAR CONTACTO");
			lblNewLabel_1_1_2_1_1_1_1_1_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
			lblNewLabel_1_1_2_1_1_1_1_1_1.setBounds(142, 552, 266, 20);
			frmAyuda.getContentPane().add(lblNewLabel_1_1_2_1_1_1_1_1_1);
			
			JLabel agregar = new JLabel("");
			agregar.setBounds(82, 540, 50, 50);
			crearImagen(agregar, "add abm.png");
			frmAyuda.getContentPane().add(agregar);
		}
		else {
			frmAyuda.setTitle("Ayuda editar persona");
			
			JLabel lblNewLabel_1_1_2_1_1_1_1_1_1 = new JLabel("CONFIRMAR EDICION");
			lblNewLabel_1_1_2_1_1_1_1_1_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
			lblNewLabel_1_1_2_1_1_1_1_1_1.setBounds(142, 552, 266, 20);
			frmAyuda.getContentPane().add(lblNewLabel_1_1_2_1_1_1_1_1_1);
			
			JLabel agregar = new JLabel("");
			agregar.setBounds(82, 540, 50, 50);
			crearImagen(agregar, "edit.png");
			frmAyuda.getContentPane().add(agregar);
		}
		

		frmAyuda.setBounds(100, 100, 343, 640);
		frmAyuda.getContentPane().setLayout(null);
			
		JLabel imagenName = new JLabel("");
		imagenName.setBounds(10, 17, 32, 32);
		crearImagen(imagenName, "name.png");
		frmAyuda.getContentPane().add(imagenName);
		
		JLabel phone = new JLabel("");
		phone.setBounds(10, 66, 32, 32);
		crearImagen(phone, "phone.png");
		frmAyuda.getContentPane().add(phone);
		
		JLabel email = new JLabel("");
		email.setBounds(10, 115, 32, 32);
		crearImagen(email, "email.png");
		frmAyuda.getContentPane().add(email);
		
		JLabel maps = new JLabel("");
		maps.setBounds(10, 193, 32, 32);
		crearImagen(maps, "maps.png");
		frmAyuda.getContentPane().add(maps);
		
		JLabel home = new JLabel("");
		home.setBounds(10, 240, 32, 32);
		crearImagen(home, "home.png");
		frmAyuda.getContentPane().add(home);
		
		JLabel imagenCumple = new JLabel("");
		imagenCumple.setBounds(12, 325, 32, 32);
		crearImagen(imagenCumple, "cumple.png");
		frmAyuda.getContentPane().add(imagenCumple);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 158, 308, 7);
		frmAyuda.getContentPane().add(separator);
		
		JLabel etiqueta = new JLabel("");
		etiqueta.setBounds(10, 399, 32, 32);
		crearImagen(etiqueta, "label.png");
		frmAyuda.getContentPane().add(etiqueta);
		
		JLabel club = new JLabel("");
		club.setBounds(40,473,32,32);
		crearImagen(club, "club.png");
		frmAyuda.getContentPane().add(club);
		
		JLabel lblDomicilio = new JLabel("Datos del domicilio");
		lblDomicilio.setBounds(20, 166, 139, 14);
		frmAyuda.getContentPane().add(lblDomicilio);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 283, 308, 7);
		frmAyuda.getContentPane().add(separator_1);
		
		JSeparator separator_1_1 = new JSeparator();
		separator_1_1.setBounds(10, 372, 307, 7);
		frmAyuda.getContentPane().add(separator_1_1);
		
		JSeparator separator_1_1_1 = new JSeparator();
		separator_1_1_1.setBounds(10, 442, 308, 7);
		frmAyuda.getContentPane().add(separator_1_1_1);
		
		JSeparator separator_1_1_1_1 = new JSeparator();
		separator_1_1_1_1.setBounds(10, 530, 308, 7);
		frmAyuda.getContentPane().add(separator_1_1_1_1);
		
		JLabel lblClubDeFutbol = new JLabel("Seleccione club de futbol:");
		lblClubDeFutbol.setBounds(20, 452, 189, 14);
		frmAyuda.getContentPane().add(lblClubDeFutbol);
		
		JLabel lblNewLabel = new JLabel("Fecha de cumplea\u00F1os");
		lblNewLabel.setBounds(20, 291, 189, 14);
		frmAyuda.getContentPane().add(lblNewLabel);
		
		JLabel lblEtiqueta = new JLabel("Etiqueta");
		lblEtiqueta.setBounds(20, 382, 71, 14);
		frmAyuda.getContentPane().add(lblEtiqueta);
		
		JLabel lblNewLabel_1 = new JLabel("INGRESAR NOMBRE DE CONTACTO");
		lblNewLabel_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1.setBounds(52, 17, 266, 32);
		frmAyuda.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("INGRESAR TELEFONO");
		lblNewLabel_1_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1_1.setBounds(52, 60, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Solo se permite numeros");
		lblNewLabel_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1.setBounds(52, 78, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_2 = new JLabel("INGRESAR CORREO ELECTRONICO");
		lblNewLabel_1_1_2.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1_1_2.setBounds(52, 110, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_2);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("El formato debe tener un . (punto) y una @ (arroba)");
		lblNewLabel_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1.setBounds(52, 127, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1);
		
		JLabel lblNewLabel_1_1_2_1 = new JLabel("AGREGAR. EDITAR Y BORRAR LOCALIDAD");
		lblNewLabel_1_1_2_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1_1_2_1.setBounds(52, 190, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_2_1);
		
		JLabel lblNewLabel_1_1_1_1_1 = new JLabel("Puede ingresar un pais, una provincia y una localidad");
		lblNewLabel_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1.setBounds(52, 205, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1_1);
		
		JLabel lblNewLabel_1_1_2_1_1 = new JLabel("INGRESAR DATOS DEL DOMICILIO\r\n");
		lblNewLabel_1_1_2_1_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1_1_2_1_1.setBounds(52, 235, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_2_1_1);
		
		JLabel lblNewLabel_1_1_1_1_1_1 = new JLabel("El campo Altura solo permite n\u00FAmeros\r\n");
		lblNewLabel_1_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1_1.setBounds(52, 252, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1_1_1);
		
		JLabel lblNewLabel_1_1_2_1_1_1 = new JLabel("INGRESAR DIA Y MES DE CUMPLEA\u00D1OS");
		lblNewLabel_1_1_2_1_1_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1_1_2_1_1_1.setBounds(52, 307, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_2_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1_1_1_1 = new JLabel("El campo dia permite hasta dos numeros ");
		lblNewLabel_1_1_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1_1_1.setBounds(54, 325, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1_1_1_1_1 = new JLabel("El mes se elige desde un desplegable");
		lblNewLabel_1_1_1_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1_1_1_1.setBounds(54, 341, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1_1_1_1_1);
		
		JLabel lblNewLabel_1_1_2_1_1_1_1 = new JLabel("AGREGAR, BORRAR EDITAR ETIQUETAS");
		lblNewLabel_1_1_2_1_1_1_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1_1_2_1_1_1_1.setBounds(52, 393, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_2_1_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1_1_1_1_1_1 = new JLabel("Podra elegir una etiqueta de una lista desplegable\r\n");
		lblNewLabel_1_1_1_1_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1_1_1_1_1.setBounds(52, 411, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1_1_1_1_1_1);
		
		JLabel lblNewLabel_1_1_2_1_1_1_1_1 = new JLabel("AGREGAR CLUB DE FUTBOL");
		lblNewLabel_1_1_2_1_1_1_1_1.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblNewLabel_1_1_2_1_1_1_1_1.setBounds(82, 474, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_2_1_1_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1_1_1_1_1_1_1 = new JLabel("Al presionar el balon podra ingresar una liga y un club");
		lblNewLabel_1_1_1_1_1_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1_1_1_1_1_1.setBounds(52, 504, 266, 20);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1_1_1_1_1_1_1);
		

				
	}
	
	public void crearImagen(JLabel label, String ruta) 
	{
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
}
