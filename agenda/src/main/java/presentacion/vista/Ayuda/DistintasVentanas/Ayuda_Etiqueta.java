package presentacion.vista.Ayuda.DistintasVentanas;

import java.awt.Font;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ayuda_Etiqueta 
{
	private JFrame frmAyuda;

	public Ayuda_Etiqueta(JFrame frame) 
	{
		frmAyuda = frame;
		frmAyuda.setTitle("Ayuda de tipo de contactos");
		frmAyuda.setBounds(100, 100, 369, 300);
		frmAyuda.getContentPane().setLayout(null);
		
		JLabel Agregar = new JLabel("");
		Agregar.setBounds(25, 11, 40, 40);
		crearImagen(Agregar, "add abm.png");
		frmAyuda.getContentPane().add(Agregar);
		
		JLabel Editar = new JLabel("");
		Editar.setBounds(25, 62, 40, 40);
		crearImagen(Editar, "edit.png");
		frmAyuda.getContentPane().add(Editar);
		
		JLabel Borrar = new JLabel("");
		Borrar.setBounds(25, 113, 40, 40);
		crearImagen(Borrar, "delete.png");
		frmAyuda.getContentPane().add(Borrar);
		
		JLabel Guardar = new JLabel("");
		Guardar.setBounds(25, 164, 40, 40);
		crearImagen(Guardar, "save.png");
		frmAyuda.getContentPane().add(Guardar);
		
		JLabel lblEditarContacto = new JLabel("EDITAR ETIQUETA");
		lblEditarContacto.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblEditarContacto.setBounds(90, 57, 147, 23);
		frmAyuda.getContentPane().add(lblEditarContacto);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAyuda.dispose();
			}
		});
		btnNewButton.setBounds(151, 227, 59, 23);
		frmAyuda.getContentPane().add(btnNewButton);
		
		JLabel lblAgregarContacto = new JLabel("AGREGAR ETIQUETA");
		lblAgregarContacto.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblAgregarContacto.setBounds(90, 11, 147, 23);
		frmAyuda.getContentPane().add(lblAgregarContacto);
		
		JLabel lblEliminarEtiqueta = new JLabel("ELIMINAR ETIQUETA");
		lblEliminarEtiqueta.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblEliminarEtiqueta.setBounds(90, 110, 147, 23);
		frmAyuda.getContentPane().add(lblEliminarEtiqueta);
		
		JLabel lblGuardarEtiqueta = new JLabel("GUARDAR ETIQUETA");
		lblGuardarEtiqueta.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		lblGuardarEtiqueta.setBounds(90, 160, 147, 23);
		frmAyuda.getContentPane().add(lblGuardarEtiqueta);
		
		JLabel lblNewLabel_1_1 = new JLabel("Seleccionar una etiqueta para editarlo.");
		lblNewLabel_1_1.setBounds(90, 80, 256, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Seleccionar una etiqueta para eliminarlo.\r\n");
		lblNewLabel_1_1_1.setBounds(90, 133, 256, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1 = new JLabel("Abre una nueva ventana, para ingresar etiqueta");
		lblNewLabel_1.setBounds(90, 33, 253, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("Guarda etiqueta y cierra la ventana");
		lblNewLabel_1_1_1_1.setBounds(90, 181, 256, 14);
		frmAyuda.getContentPane().add(lblNewLabel_1_1_1_1);
	}
	
	public void crearImagen(JLabel label, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
}
