package presentacion.vista.Ayuda;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import presentacion.vista.Ayuda.DistintasVentanas.Ayuda_Etiqueta;
import presentacion.vista.Ayuda.DistintasVentanas.Ayuda_VentanaPersona;
import presentacion.vista.Ayuda.DistintasVentanas.Ayuda_Vista;
import presentacion.vista.Ayuda.DistintasVentanas.Ayuda_VistaClubes;

public class Ayuda {

	private JFrame frmAyuda;

	/**
	 * Create the application.
	 */
	public Ayuda(String accion) {
		initialize(accion);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String accion) {
		frmAyuda = new JFrame();
		
		if(accion.equals(ConstanteAyuda.VISTA)) {
			new Ayuda_Vista(frmAyuda);	
		} else if(accion.equals(ConstanteAyuda.VENTANAPERSONA_AGREGAR)) {
			new Ayuda_VentanaPersona(frmAyuda, ConstanteAyuda.VENTANAPERSONA_AGREGAR);
		} else if(accion.equals(ConstanteAyuda.VENTANAPERSONA_EDITAR)) {
			new Ayuda_VentanaPersona(frmAyuda, ConstanteAyuda.VENTANAPERSONA_EDITAR);
		}else if(accion.equals(ConstanteAyuda.VISTACLUBES)) {
			new Ayuda_VistaClubes(frmAyuda);
		} else if(accion.equals(ConstanteAyuda.ETIQUETA)) {
			new Ayuda_Etiqueta(frmAyuda);
		}
	}
	
	public void show()
	{
		this.frmAyuda.setVisible(true);
	}
	
	public void crearImagen(JLabel label, String ruta) {
		ImageIcon Imagen = new ImageIcon(getClass().getResource("/Imagenes/"+ruta));
		ImageIcon Icono = new ImageIcon(Imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(Icono);
	}
}
