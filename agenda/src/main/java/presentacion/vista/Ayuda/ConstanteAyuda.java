package presentacion.vista.Ayuda;

public class ConstanteAyuda {
    private ConstanteAyuda() {
    }
    
    public static final String VISTA = "Vista";
    public static final String VENTANAPERSONA_AGREGAR = "Ventana persona - agregar";
    public static final String VENTANAPERSONA_EDITAR = "Ventana persona - editar";
    public static final String VISTACLUBES = "Vista Clubes";
    public static final String LOCALIDADES = "VistaABMLocalidades";
    public static final String ETIQUETA = "VistaTiposContacto";

}
