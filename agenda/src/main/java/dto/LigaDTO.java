package dto;

public class LigaDTO {
	private int idLiga;
	private String liga;
	
	public LigaDTO(int idLiga, String liga) 
	{
		this.idLiga = idLiga;
		this.liga = liga;
	}
	
	public int getIdLiga() 
	{
		return idLiga;
	}

	public void setIdLiga(int idLiga) 
	{
		this.idLiga = idLiga;
	}

	public String getNombre() 
	{
		return liga;
	}

	public void setNombre(String nombre) 
	{
		this.liga = nombre;
	}
	
	public String toString()
	{
		return this.getNombre();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idLiga;
		result = prime * result + ((liga == null) ? 0 : liga.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LigaDTO other = (LigaDTO) obj;
		if (idLiga != other.idLiga)
			return false;
		if (liga == null) {
			if (other.liga != null)
				return false;
		} else if (!liga.equals(other.liga))
			return false;
		return true;
	}
}

