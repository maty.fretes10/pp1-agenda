package dto;

public class PaisDTO 
{
	private int id;
	private String nombre;
	
	public PaisDTO (int _id, String _nombre)
	{
		this.id = _id;
		this.nombre = _nombre;	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
    public String toString() {
        return this.nombre;
    }
	
    @Override  
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaisDTO)) {
            return false;
        }
        PaisDTO provincia = (PaisDTO) o;
        if (id != provincia.id) {
            return false;
        }
        if (nombre != null ? !nombre.equals(provincia.nombre) : provincia.nombre != null) {
            return false;
        }
        return true;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.nombre != null ? this.nombre.hashCode() : 0);
        hash = 89 * hash + this.id;
        return hash;
    }

}
