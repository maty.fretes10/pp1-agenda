package dto;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String _etiqueta;
	private DomicilioDTO dom;
	private String correo;
	private int dia;
	private int mes;
	private String club;
	private String liga;


	public PersonaDTO(int idPersona, String nombre, String telefono, String etiqueta, DomicilioDTO domicilio, String correo, int dia, int mes, String club, String liga)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this._etiqueta = etiqueta;
		this.dom = domicilio;
		this.correo = correo;
		this.dia = dia;
		this.mes = mes;
		this.club = club;
		this.liga = liga;
	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}
	
	public String get_etiqueta() 
	{
		return _etiqueta;
	}

	public void set_etiqueta(String _etiqueta) 
	{
		this._etiqueta = _etiqueta;
	}

	public DomicilioDTO getDom() {
		return dom;
	}

	public void setDom(DomicilioDTO dom) {
		this.dom = dom;
	}
	
	public String getCorreo() {
		return this.correo;
	}
	
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public int getDia() {
		return this.dia;
	}
	
	public void setDia(int dia) {
		this.dia = dia;
	}
	
	public int getMes() {
		return this.mes;
	}
	
	public void setMes(int mes) {
		this.mes = mes;
	}
	
	public String getClub() {
		return club;
	}
	
	public void setClub(String club) {
		this.club = club;
	}
	
	public String getLiga() {
		return liga;
	}
	
	public void setLiga(String liga) {
		this.liga = liga;
	}

}
