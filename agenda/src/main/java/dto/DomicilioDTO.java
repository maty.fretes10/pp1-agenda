package dto;


public class DomicilioDTO 
{
	private int idpersona;
	private String calle;
	private int altura;
	private int piso;
	private int depto;
	private String localidad;
	private String provincia;
	private String pais;
	
	public DomicilioDTO(int id, String _calle, int _altura, int _piso, int _depto, String _localidad, String _provincia, String _pais) 
	{
		this.setIdpersona(id);
		this.calle = _calle;
		this.altura = _altura;
		this.piso = _piso;
		this.depto = _depto;
		this.localidad = _localidad;
		this.provincia = _provincia;
		this.pais = _pais;

	}


	public String getCalle() 
	{
		return calle;
	}


	public void setCalle(String calle) 
	{
		this.calle = calle;
	}


	public int getAltura() 
	{
		return altura;
	}


	public void setAltura(int altura) 
	{
		this.altura = altura;
	}


	public int getPiso() 
	{
		return piso;
	}


	public void setPiso(int piso) 
	{
		this.piso = piso;
	}


	public int getDepto() 
	{
		return depto;
	}


	public void setDepto(int depto) 
	{
		this.depto = depto;
	}


	public int getIdpersona() 
	{
		return idpersona;
	}


	public void setIdpersona(int idpersona) 
	{
		this.idpersona = idpersona;
	}


	public String getLocalidad() {
		return localidad;
	}


	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}


	public String getProvincia() {
		return provincia;
	}


	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}


	public String getPais() {
		return pais;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}

	

	
	
	

}
