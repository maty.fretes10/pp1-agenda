package dto;

public class ClubDTO  implements Comparable <ClubDTO>
{
	private int idLiga;
	private int idClub;
	private String club;
	
	public ClubDTO(int idLiga, int idClub, String club) 
	{
		this.idLiga = idLiga;
		this.idClub = idClub;
		this.club = club;
	}
	
	public int getIdLiga() 
	{
		return idLiga;
	}

	public void setIdLiga(int idLiga) 
	{
		this.idLiga = idLiga;
	}

	public int getIdClub() 
	{
		return idClub;
	}

	public void setIdClub(int idClub) 
	{
		this.idClub = idClub;
	}

	public String getNombreClub() 
	{
		return club;
	}

	public void setNombreClub(String nombre) 
	{
		this.club = nombre;
	}
	
	public String toString()
	{
		return this.getNombreClub();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idClub;
		result = prime * result + ((club == null) ? 0 : club.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClubDTO other = (ClubDTO) obj;
		if (idClub != other.idClub)
			return false;
		if (club == null) {
			if (other.club != null)
				return false;
		} else if (!club.equals(other.club))
			return false;
		return true;
	}
	public int compareTo(ClubDTO o) {
		return this.club.compareTo(o.getNombreClub());
	}
}