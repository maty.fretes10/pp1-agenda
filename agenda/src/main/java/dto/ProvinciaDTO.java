package dto;


public class ProvinciaDTO {

	private int idpais;
    private int id;
    private String nombre;
    
    public ProvinciaDTO(int idpais, int id, String prov) {
    	this.idpais = idpais;
    	this.id = id;
    	this.nombre = prov;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    

    public int getIdpais() {
		return idpais;
	}

	public void setIdpais(int idpais) {
		this.idpais = idpais;
	}

	@Override
    public String toString() {
        return this.nombre;
    }


}
