package dto;

public class LocalidadDTO implements Comparable <LocalidadDTO>
{

    private int id;
    private int id_prov;
    private String nombre;
    
    public LocalidadDTO(int id, int id_prov, String loc) 
    {
    	this.id = id;
    	this.nombre = loc;
    	this.id_prov = id_prov;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    

    public int getId_prov() {
		return id_prov;
	}

	public void setId_prov(int id_prov) {
		this.id_prov = id_prov;
	}
	
	@Override
    public String toString() {
        return this.nombre;
    }
	public int compareTo(LocalidadDTO o) {
		return this.nombre.compareTo(o.getNombre());
	}



}